#Innehåll

Blandade uppgifter från Java YA och lite annat testande i olika API:er.


###HelloWorld
Basic app som printar "Hello World" i Terminalen via System.out.println(" ")

###IverteraNummer
Räknar upp 1-20 och sedan ner igen. 
Använder; for loop, Scanner och System.out.println()

###Nummer
Räknar upp till 20 från ett nummer under 1-20.
Använder; for loop, Scanner och System.out.println()

###NamnListan ( vanliga arrays )
Skriv in namn och telefon, stoppa loopen, printa värden.
Använder; do, while, for, arrays, booleans, Scanner, if/else/else if etc.

###[Yatzy](https://bitbucket.org/lano78/uppgifter/src/98a286ad14221122bc825618a074d6f0458927d0/src/Yatzy.java?at=master)
Yatzy, ingen GUI. Spelas i CMD.
Använder; do, while, for, arrays, Scanner, if/else/else if etc.