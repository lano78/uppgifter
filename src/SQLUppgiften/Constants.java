/**
 * 
 */
package SQLUppgiften;

/**
 * @author Lars Nordström
 *
 */
public interface Constants {
	
	// is it a remote db?
	public final boolean IS_REMOTE_DB = false; 
	// Driver
	public final String DB_CLASS_NAME = "com.mysql.jdbc.Driver";
	// local login
	public final String LOCAL_USERNAME = "root";
	public final String LOCAL_PASSWORD = "";
	
	// Local connection
	public final String DB_NAME = "EmployeeDB";
	public final String LOCAL_URL = "127.0.0.1";//"10.63.0.131";

	// Remote Connection
	public final String MY_IP ="10.0.2.15";//"192.168.1.82";// "192.168.1.64";//"10.0.2.15";
	public final String REMOTE_USERNAME = "HP-640-Ubuntu.yajo.local";// "mbp.lan";
	public final String REMOTE_PASSWORD = "elev";
	public final String SERVER_IP = "10.63.0.131";//"192.168.1.64"; //"10.63.0.131";
	public final String PORT = "3306";
	
	// Local mysql db
	public final String LOCAL_DB_CONNECTION = "jdbc:mysql://" + LOCAL_URL + "/" + DB_NAME;
	
	// Shared mysql on local network.
	public final String REMOTE_DB_CONNECTION = "jdbc:mysql://" + SERVER_IP + ":" + PORT + "/" + DB_NAME + "?user=" + REMOTE_USERNAME + "&password=" + REMOTE_PASSWORD;
	
	// Columns
	public String [] COLUMNS = {"emp_id", "fname", "lname", "start_date", "end_date", "superior_emp_id", "dept_id", "title", "assigned_branch_id"};

	// Queries
	public String BEGIN_WITH = "%');"; //"('?%');";
	public String ENDS_WITH = "%?';";
	public String CONTAINS = "%?%';";
	
	public String F_NAME = "SELECT * FROM employee WHERE fname LIKE UPPER('";
	public String L_NAME = "SELECT * FROM employee WHERE lname LIKE '";
	
	// Main menu Queries
	public String ALL_QUERY = "SELECT * FROM employee;";
	public String F_NAME_QUERY = "SELECT emp_id, fname, lname FROM employee WHERE fname LIKE UPPER('?%');";
	public String L_NAME_QUERY = "SELECT emp_id, fname, lname FROM employee WHERE lname LIKE UPPER('?%');";
	public String DEPARTMENT_QUERY = "SELECT emp_id, fname, lname, dept_id FROM employee WHERE dept_id='?';";
	public String MANAGER_QUERY = "SELECT emp_id, fname, lname, superior_emp_id FROM employee WHERE superior_emp_id='?';";
	
	public String PERSON_QUERY = "SELECT * FROM employee WHERE emp_id=1;";
	
	//public String[] QUERIES = {"SELECT * FROM employee;", "SELECT emp_id, fname, lname FROM employee WHERE fname LIKE UPPER('?%');", "SELECT emp_id, fname, lname FROM employee WHERE lname LIKE UPPER('?%');"};
	public String[] QUERIES = {ALL_QUERY, F_NAME_QUERY, L_NAME_QUERY, DEPARTMENT_QUERY, MANAGER_QUERY};

	// UI
	public String[] MAIN_MENU_LABELS = {"All employees", "First name",  "Last name", "Department ID", "Manager ID", "Avsluta"};

}


