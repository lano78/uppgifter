/**
 * 
 */
package SQLUppgiften;

/**
 * @author Lars Nordström
 *
 */
public class Menu implements Constants{
	
	private final String[] menuLabels = MAIN_MENU_LABELS ;
	/**
	 * Simple menusystem.<br>
	 * Takes an array of Strings []items.
	 * @param
	 */
	public void mainMenu(String [] items){
		// print the menu items on the screen.
		System.out.println(" Huvudmeny");
		System.out.println("+-------------------+");
		for (int i = 0; i <= (items.length - 1); i++){
			System.out.println( (i+1) + ". "+ items[i]);
		}
		System.out.println("+-------------------+");
		System.out.println(" Gör ditt val: 1-" + items.length );
	}
	
	public void editMenu(){
		System.out.println("Ändra? (j/n)");
	}
	
	public void editItemMenu(String [] items){
		System.out.println("+-------------------+");
		for (int i = 0; i <= (items.length - 1); i++){
			System.out.println( (i+1) + ". "+ items[i]);
		}
		System.out.println("+-------------------+");
		System.out.println("Vad vill du ändra: 1-" + items.length  +" ");
	}
	public void exitMenu(){
		System.out.println("Avsluta? (j/n)");
		
	}
	public int menuItemsCount(String [] items){
		return items.length;
	}
	public String getMenuSelection(int i){
		return this.menuLabels[i];
	}
}
