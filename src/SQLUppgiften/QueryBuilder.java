/**
 * Use: QueryBuilder queryBuilder = new QueryBuilder();
 * 		
 * 			StringBuilder sql = queryBuilder.getQuery();
 * 
 */
package SQLUppgiften;

/**
 * @author Lars Nordström
 *
 */ 
import java.util.ArrayList;

public class QueryBuilder {
	//========================================================
	// QueryCache
	//========================================================

	/**
	 * String queryCache<br>
	 * Cache for queries.<br>
	 *  private variable used internally.<br> 
	 *  Use getQueryCache() to get value of the cache.<br>
	 */
	private StringBuilder cache = new StringBuilder();
	
	/**
	 * Array<br>
	 * Save history of queries entered.
	 */
	private static ArrayList<String> queryHistory = new ArrayList<String>();
	
	/**
	 * Append strings to query.<br>
	 * @param
	 * Appends a string to the queryCache.<br>
	 */
	public void appendToQuery(String str){
		cache.append(str);
	}
	/**
	 * Returns the query as a string.
	 * @return queryCache
	 */
	public StringBuilder getQuery(){
		return cache;
	}
	/**
	 * Clear the queryCache.
	 * @return nothing.
	 */
	public void clearQuery(){
		addToQueryHistory(cache.toString());
		cache.delete(0, cache.length());
	}
	
	/**
	 * Print the current queryCache.
	 * @return nothing. 
	 */
	public void printQuery(){
		System.out.println(" ==>> Query : " + cache.toString() );
	}
	
	// Replace all spaces with "%20"
	public String prepareQueryForURL(String str){
		return str.replaceAll(" ", "%20");
	}
	
	// Add a query to queryHistory
	private static void addToQueryHistory(String str){
		queryHistory.add(str);
	}
	
	// Clear queryHistory
	public void clearQueryHistory(){
		queryHistory.clear();
	}
	
	// Returns a query from history
	public String getQueryFromHistory(int i){
		return queryHistory.get(i);
	}
	
	// Print all queries in history
	public void printQueryHistory(){
		for(int i = 0; i <= queryHistory.size(); i++){
			System.out.println(i + ". " + getQueryFromHistory(i) );
		}
	}
	
	//
	
}




