package SQLUppgiften;

import java.util.Scanner;

public class InputHandler {
	Scanner scanner = new Scanner(System.in);
	
	// input scanner for strings.
	public String inputString(){
		// Initiate scanner for strings.
		String input = scanner.nextLine();
		// return the input.
		return input;
	}
	// 
	public String next(){
		return scanner.next();
	}
	// input scanner for integers.
	public int inputInt(){
		// initiate scanner for integers.
		int input = scanner.nextInt();
		// return the input.
		return input;
	}
	// 
	public Object userInput(){
		Object result = scanner.nextLine();
		if ( scanner.nextLine().equals("exit") ){
			exit();
		}
		return result;
	}
	// (String)userInput()
	// close the scanner to avoid memoryleak.
	public void exit(){
		scanner.close(); 
		scanner = null;
		System.out.println(" ===>> Scanner closed.");
	}
	
}




