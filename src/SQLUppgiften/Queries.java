/**
 * 
 */
package SQLUppgiften;

/**
 * @author elev
 *
 */
public interface Queries {
	// Keywords
		public String CREATE 	= "CREATE ";
		public String SELECT 	= "SELECT ";
		public String FROM 		= "FROM ";
		public String WHERE 	= "WHERE ";
		public String ALTER 	= "ALTER ";
		public String DROP 		= "DROP ";
		public String MODIFY 	= "MODIFY ";
		public String AS 		= "AS ";
		public String DESC 		= "DESC ";
		public String JOIN 		= "JOIN ";
		public String TABLE 	= "TABLE ";
		public String SET 		= "SET ";
		public String IF_EXISTS = "IF EXISTS ";
		public String NOT_NULL  = "NOT NULL ";
		public String PK 		= "PRIMARY KEY ";
		public String AUTO_INC  = "AUTO_INCREMENT ";
		public String USER 		= "USER ";
		public String ID 		= "IDENTIFIED BY ";
		
		//
		public String CONTAINS = "'%?%' ";
		public String BEGIN_WITH = "'?%' ";
		public String ENDS_WITH = "'%?' ";
		
		// Statements
		public String CREATE_DB   = "CREATE DATABASE ";			// + "dbName;"
		public String SELECT_FROM = "SELECT * FROM "; 			// + "tableName;"
		public String ALTER_TABLE = "ALTER TABLE "; 			// + "tableName;"
		public String GET_DESC    = "DESC "; 					// + "tableName;"
		public String DROP_TABLE  = "DROP TABLE IF EXISTS "; 	// + "tableName;"
		
		// Before dropping a table
		public String SET_FK_CHECK = "SET FOREIGN_KEY_CHECKS = 0;";
		// After dropping a table 
		//public String SET_FK_CHECK = "SET FOREIGN_KEY_CHECKS = 1;";
		
		//Tables in current db.
		public String SHOW_TABLES = "SHOW TABLES";
			
		// List all users of the database.
		public String GET_USERS = "SELECT host, user, password FROM mysql.user;";
		
		// get a specific user.
		public String GET_USER = "SELECT host, user, password FROM mysql.user WHERE name="; // + "'some name';"
		
		public String CREATE_USER = "CREATE USER '[MY USERNAME]'@'[IP address]' IDENTIFIED BY '[password]';";
		
		public String DROP_USER = "DROP USER 'username'@'userIP';";
		
		// "CREATE USER 'HP-640-Ubuntu'@'%.lan' IDENTIFIED BY 'elev';";
		// "GRANT ALL PRIVILEGES ON *.* TO 'HP-640'@'%.lan'@'%.lan';";
		// "FLUSH PRIVILEGES;";
		
		// "CREATE USER 'HP-640'@'%.lan' IDENTIFIED BY 'elev';";
		// "GRANT ALL PRIVILEGES ON *.* TO 'HP-640'@'%.lan'@'%.lan';";
		// "FLUSH PRIVILEGES;";
		
}
