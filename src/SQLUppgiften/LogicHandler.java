/**
 * 
 */
package SQLUppgiften;
import java.util.Scanner;

/**
 * @author Lars Nordström
 *
 */
public class LogicHandler implements Constants, Queries{
	
	static InputHandler inputHandler = new InputHandler();
	static DbHandler dbHandler = new DbHandler();
	
	QueryBuilder queryBuilder = new QueryBuilder();
	
	static Menu menu = new Menu();
	
	//static InputHandler input = new InputHandler();
	Scanner scanner = new Scanner(System.in);
	
	public LogicHandler(){
		init();
	}
	/**
	 * public
	 * @param
	 * 
	 */
	//@SuppressWarnings("finally")
	public void init(){
		
		dbHandler.connect();
		
		String [] columns = dbHandler.getColumnNames("employee");
		
		String input ;
	
		// App loop.
		do{
			
					menu.mainMenu(MAIN_MENU_LABELS);
					input = inputHandler.inputString();
					
					StringBuilder sql = queryBuilder.getQuery();
					// Clear the query cache
					queryBuilder.clearQuery();
					
					switch(input){
					case "1":
						
						getAllEmployees(input);
						
						break;
						
					case "2":
						getEmployeeByFirstName(input, sql, columns);
						break;
						
					case "3":
						dbHandler.queryDB(ALL_QUERY);
						
						queryBuilder.appendToQuery("SELECT * FROM employee WHERE lname LIKE UPPER('" );
						
						System.out.println("Välj en bokstav (a-z)");
						
						input = inputHandler.inputString();
						queryBuilder.appendToQuery( input + "%');" );
						
						sql = queryBuilder.getQuery();
						dbHandler.queryDB(sql.toString());
						
						break;
						
					case "4":
						
						break;
						
					case "5":
						
						break;
						
					case "6":
						
						exit();
						
						break;
					}

			
		}while(!input.equals("exit") || !input.equals("6"));
		
		exit();
	}
	
	// Exit the program and disconnect the db.
	private void exit(){
		dbHandler.disconnect();
		System.out.println("Avslutar....");
		System.exit(0);
		
	}
	
	// Query menu case 1
	private void getAllEmployees(String input){
		dbHandler.queryDB(ALL_QUERY);
		
		menu.exitMenu();
		
		input = inputHandler.inputString();
		
		if(input.equals("j")){
			exit();
		}
	}
	
	private void getEmployeeByFirstName(String input, StringBuilder sql, String[]columns){

		// Print all employees
		dbHandler.queryDB(ALL_QUERY);
		queryBuilder.appendToQuery("SELECT * FROM employee WHERE fname LIKE UPPER('" );
		
		System.out.println("Välj en bokstav (a-z)");
		
		input = inputHandler.inputString();
		queryBuilder.appendToQuery( input + "%');" );
		
		sql = queryBuilder.getQuery();
		dbHandler.queryDB(sql.toString());
		
		 menu.editMenu();
		 input = inputHandler.inputString();
		 if(input.equals("j")){
			 
			 queryBuilder.clearQuery();
			 
			 sql = queryBuilder.getQuery();
			 
			 menu.editItemMenu(columns);
			 
			 input = inputHandler.inputString();
			 String num = input;
			 
			 queryBuilder.appendToQuery("UPDATE TABLE employee SET " + columns[Integer.parseInt(input) - 1] + "=");
			 System.out.println("Ändra " + columns[Integer.parseInt(input) - 1] + " till?");
			 
			 input = inputHandler.inputString();
			 queryBuilder.appendToQuery("'" + input + "' WHERE emp_id='" + num + ";");
			 
			 dbHandler.queryDB(sql.toString());
			 
		}else{
			//break;
		}
		//menu.exitMenu();
		
		input = inputHandler.inputString();
		if(input.equals("j")){
			exit();
		}
		// Clear the queryCache.
		queryBuilder.clearQuery();
		
	}
	
	
}





