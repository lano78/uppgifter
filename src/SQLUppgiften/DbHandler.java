/**
 * 
 */
package SQLUppgiften;

import java.sql.*;
import java.util.ArrayList;


/**
 * @author Lars Nordström
 *
 */
public class DbHandler implements Constants, Queries {
	
	Connection connection = null;
	Statement stmt = null;
	

	//========================================================
	// QueryMethods
	//========================================================
	
	/**
	 * getColumnNames(String table)
	 * @param table - takes a table from the db.
	 * @return Array of column names and print out the contents.
	 */
	public String[] getColumnNames(String table){
		
		String colNames [] = null;
		
		try{
				// Check if db is open, open if not.
				if ( !checkConn() ){
					//openDB();
				}
				String query = SELECT_FROM + table;
				// 
				Statement stmt = connection.createStatement();
				//
				ResultSet result = stmt.executeQuery(query);
				
				// Get metadata from table
				ResultSetMetaData meta = result.getMetaData();
				// Array for column names.
				colNames = new String[meta.getColumnCount()];
					//  loop the columns.
					for (int i = 1; i <= meta.getColumnCount(); i++ ){
						// insert the column names to an array.
						colNames[i-1] = meta.getColumnName(i);
					
						//System.out.println(i + " = " + colNames[i-1] );
						
					}
					//System.out.println();
			
				result.close();
				stmt.close();
		}catch(SQLException exception){
				exception.printStackTrace();
			}
			System.out.println();
			//closeDB();	
			return colNames;
	}
	
	/** 
	 * Takes a String and print the results to console.
	 * @param query takes a string.
	 */
	public void queryDB(String query){
		try{
			// Check if db is open, open if not.
			if ( !checkConn() ){
				//openDB();
			}
			// 
			Statement stmt = connection.createStatement();
			//
			ResultSet result = stmt.executeQuery(query);
			
			// Get metadata from table
			ResultSetMetaData meta = result.getMetaData();
			
			// Get num columns in db
			int colCnt = meta.getColumnCount();
			
					
			// Print the result from the query.
			while(result.next()){
				// loop the columns
				System.out.println();
				for (int i = 1; i <= colCnt; i++ ){
					Object value = result.getObject(i);
					System.out.print(value);
					System.out.print("\t");
				}
				System.out.println();
			}
			result.close();
			stmt.close();
		}catch(SQLException exception){
			exception.printStackTrace();
		}
		System.out.println();
		//closeDB();	
	}
	
	//========================================================
	// Database
	//========================================================
	
	/**
	 * Connects to a database 
	 */
	public void connect(){
		// Check if db is open, open if not.
		if ( !checkConn() ){
			openDB();
		}

	}
	
	/**
	 * Disconnect the current database.
	 */
	public void disconnect(){
		if ( !checkConn() ){
			closeDB();
		}
	}
	
	/**
	 * Private method.
	 * Check if there is an db connection
	 * @return 
	 * true if there is a connection to a database otherwise return false.
	 */
	private boolean checkConn(){

		try{
			if(this.connection != null){
				//System.out.println(" ===>> Connection to database OK  " + DB_NAME);
				return true;
			}else{
				//System.out.println(" ===>> Not connected to " + DB_NAME);
				return false;
			}
		}finally{
			
		}
	}

	
	// initiate connection to db.
	/**
	 * Opens a database
	 */
	private void openDB(){

		// if not remote, use local user info.
		String username = LOCAL_USERNAME;
		String password = LOCAL_PASSWORD;
		String db_conn  = LOCAL_DB_CONNECTION;
		String db_type = "local";
		// if remote, use remote user info.
		if (IS_REMOTE_DB) {
			username = REMOTE_USERNAME;
			password = REMOTE_PASSWORD;
			db_conn  = REMOTE_DB_CONNECTION;
			db_type = "remote";
		}
		try {
			Class.forName(DB_CLASS_NAME);
			connection = DriverManager.getConnection(db_conn, username, password);
			System.out.println(" ===>> Connected to " + db_type + " database : " + DB_NAME);
		} catch (ClassNotFoundException | SQLException exception) {
			
			exception.printStackTrace();
		}
	}
	
	// Close the DB if it is open.
	/**
	 * Close the current database if open.
	 */
	private void closeDB(){
		try{
			// Check if db is open.
			if ( checkConn() ){
				connection.close();
				System.out.println(" ===>> Disconnected from database :  " + DB_NAME);
			}
			
		}catch(SQLException exception){
			exception.printStackTrace();
		}
	}

	
}



