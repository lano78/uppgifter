
import java.io.File;
import java.util.Date;

public interface Device {
	
	// Check
	String OS = System.getProperty("os.name").toLowerCase();
	
	// Check OS version
	String OS_VERSION = System.getProperty("os.version");
	
	// Check what platform
	boolean IS_WIN = (OS.indexOf("win") >= 0);
	boolean IS_MAC = (OS.indexOf("mac") >= 0);
	boolean IS_SOLARIS = (OS.indexOf("sunos") >= 0);
	boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	
	// Get directory.
	String CURRENT_DIR = System.getProperty("user.dir");
	
	//
	String SEPARATOR = System.getProperty("file.separator");
	
	// Date 
	Date DATE_NOW = new Date();
	
}










