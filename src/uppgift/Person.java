package uppgift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;



public class Person extends Object {
	String className = "Person";
	String fileName = "Person.java"; 
	
	private String gender;
	private String fname;
	private String lname;
	private String email;
	private String phone;
	
	
	public Person(String fname, String lname, String email, String phone) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.phone = phone;
		super.setType("Människa");
	}
	public Person(){
		this.fname = "";
		this.lname = "";
		this.email = "";
		this.phone = "";
	}
	/**
	 * @return gender:String
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * Get the fname of person.
	 * @return string
	 */
	public String getfName() {
		return fname;
	}
	/**
	 * Set the fname of the person.
	 * @param fname (String)
	 */
	public void setfName(String fname) {
		this.fname = fname;
	}
	/**
	 * Get the lname of the person.
	 * @return string
	 */
	public String getLName() {
		return lname;
	}
	/**
	 * Set the lname of the person.
	 * @param lname (String)
	 */
	public void setLName(String lname) {
		this.fname = lname;
	}
	/**
	 * Get the email of the person.
	 * @return string
	 */
	public String getMail() {
		return email;
	}
	/**
	 * Set the email of the person.
	 * @param email (String)
	 */
	public void setMail(String email) {
			this.email = email;
		
	}
	/**
	 * Get the phone of person.
	 * @return string
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * Set the phone of the person.
	 * @param phone (String)
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * Get fname, lname, email, phone of person as a string.
	 * @return string
	 */
	public String getPersonInfo(){
		return super.getType() + " " + this.fname + ", " + this.lname + ", " + this.email + ", " + this.phone;
	}
	

	/**
	 * Save the person instance.
	 */
	public void save(){
		String DB_NAME = Constants.DATABASE_NAME;
		String USER_NAME = "root";
		String USER_PASSWORD = "";
		String DB_CLASS_NAME = "com.mysql.jdbc.Driver";
		String DB_CONN = "jdbc:mysql://localhost/" + DB_NAME;
		String TABLE_NAME = Constants.TABLE_NAME;
		
		String query = "INSERT INTO " + TABLE_NAME +"(fname, lname, email, phone)"
						+ " VALUES('"
						+ getfName() + "','"
						+ getLName() + "','"
						+ getMail()  + "','"
						+ getPhone() + "');";
		
		Connection connection = null;
		Statement statement = null;
				try{
					connection = DriverManager.getConnection(DB_CONN, USER_NAME, USER_PASSWORD);
					Class.forName(DB_CLASS_NAME);
					statement = connection.createStatement();
					statement.executeUpdate(query);
				
				}catch(ClassNotFoundException | SQLException exception){
					
				}finally{
				      try{
					      if(statement != null)
					        	 statement = null;
					        	 connection.close();
					      }catch(SQLException e){
					    	  e.printStackTrace();
					      }// do nothing
					      try{
					         if(connection != null )
					        	 statement = null;
					        	 connection.close();
					      }catch(SQLException e){
					         e.printStackTrace();
					      }
					
				}
	}
}


















