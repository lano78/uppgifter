package uppgift;

public class Cleanup {
	static String className = "Cleanup";
	static String fileName = "Cleanup.java";
	
	public Cleanup() {
		// TODO Auto-generated constructor stub
	}
	
	// Remove any other chars, whitespaces, tabs etc and only keep numbers.
	static String cleanAllNonNumbers(String s){
		String pattern = "[\\s+\\s\\-,_./:;a-öA-Ö]";
		return s.replaceAll(pattern, "");
	}
	
	// Remove any other chars from a string that is not a letter or a -
	static String cleanAllNonLetters(String s){
		String pattern = "[\\s+\\s\\,_./:;0-9]";
		return s.replaceAll(pattern, "");
	}

}
