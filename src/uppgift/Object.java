package uppgift;

public class Object {
	
	private String type;
	
	public Object(String t){
		this.setType(t);
	}
	public Object() {
		this.setType("not defined");
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
