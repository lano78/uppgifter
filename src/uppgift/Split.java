package uppgift;

public class Split {
	static String className = "Split";
	static String fileName = "Split.java";
	public Split() {
		// TODO Auto-generated constructor stub
	}
	// Finds any commas, semicolon, colon and replace them with a space
	static String splitCharacterSeparatedString(String s){
		return s.replaceAll("[\\,.:;_/-]", " ");
	}
	// Finds any spaces and replace them with a comma
	static String splitSpaceSeparatedString(String s){
		return s.replaceAll("[\\s]", "\\,");
	}

}
