package uppgift;

import java.util.ArrayList;

public class Data {
	
	static String className = "Data";
	static String fileName = "Data.java";
	
	private static ArrayList<String> dataArray = new ArrayList<String>();
	
	public Data() {
		
	}
	
	// add data to the arrayList
	static void setData(String s){
		dataArray.add(s);
	}
	// Return the arrayList if we need to do something
	static ArrayList<String> getAllData(){
		return dataArray;
	}
	
	// Get a specific post in the arrayList
	static String getPosInData(int i){
		return dataArray.get(i);
	}
	
	// get the number of entries in the arrayList.
	static int getCountInList(){
		return dataArray.size();
	}
	
	// Clear the data in the arrayList
	static void clearData(){
		dataArray.clear();
	}
	
	// Show the data in the arrayList and print it separated by a space.
	static void showData(){
		System.out.println("\tPersoner");
		for (String item : dataArray) {
			  System.out.println(item.replaceAll(",", " ") );
		}
		System.out.println("");
	}
}
