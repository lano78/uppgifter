package uppgift;

import java.util.ArrayList;

public class People {
	
	// Array to hold person instances.
	
	private static ArrayList<Person> people = new ArrayList<Person>(); 
	
	// Constructor not used since methods are static.
	public People(String fname, String lname, String email, String phone) {
		People.addPerson(fname, lname, email, phone);
	}
	public static ArrayList<Person> getPeopleArray(){
		return people;
	}
	/**
	 * Adds an instance to the people array.
	 * @param fname
	 * @param lname
	 * @param email
	 * @param phone
	 */
	public static void addPerson(String fname, String lname, String email, String phone){
		people.add(new Person(fname, lname, email, phone));
	}
	
	public static void addPerson(Person person){
		people.add(person);
	}
	/**
	 * Print all people in the people array.
	 */
	public static void showPeople(){
		System.out.println("\tPersoner");
		int count = 0;
		for (Person person : people) {
			  // Print each instance data
			  System.out.println(count + ". " + person.getPersonInfo() );
			  
			  // same as above.
			  //System.out.println(count + ". " + person.getfName() + ", " + person.getLName() + ", " + person.getMail() + ", " + person.getPhone());

			  count++;
		}
		System.out.println("");
	}
	
	/**
	 * Show a person instance at a index.
	 * @param idx int
	 */
	public static void showPersonAtIndex(int idx){
		System.out.println(idx + ". " + people.get(idx).getPersonInfo());
	}
	/**
	 * Get a instance at index and return it.
	 * @param idx int
	 * @return instance
	 */
	public static Person getPerson(int idx){
		return people.get(idx);
	}
	
	/**
	 * Get number of instances in the people array.
	 * @return int 
	 */
	public static int getPeopleCount(){
		return people.size();
	}

}
