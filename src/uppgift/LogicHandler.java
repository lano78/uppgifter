package uppgift;

import java.util.ArrayList;
import java.util.Scanner;



public class LogicHandler {
	
	static String className = "LogicHandler";
	static String fileName = "LogicHandler.java";
	
	static ConnectionHandler conn = new ConnectionHandler("personer", "root", "");
	static Scanner scanner = new Scanner(System.in);
	static String input;
	
	// Not using the Constructor since the start method is static and doesn't require an instance to function.
	public LogicHandler() {
		// TODO Auto-generated constructor stub

	}
	
	/**
	 * Start the app and accept input until input is "stop"
	 */
	public static void start(){
		Person person = new Person("Lars", "Page", "banan@me.com", "010101");
		person.save();
		//conn.connect();
		//People.addPerson(new Person("Blob", "Apelsin", "apple@me.com", "56789"));
		//conn.insertToDb(People.getPeopleArray(), 0);
		//conn.queryDB("SELECT * FROM elever;");
		//conn.insertBlob(People.getPeopleArray(), 0);
		
		//conn.getPerson(2);
		//System.out.println(person[0]);
		//System.out.println(Validate.validateBD("19781004-7878"));
		//System.out.println(Search.searchEmail("fjskdfsdjflfkpasdjfösdkjföfjasödfjasöfj lars@me.com","lars@me.com"));
		
		//Gui window = new Gui();
		//window.setVisible(true);
		//add();
		show();
		
	}
	
	public static void stop(){ exit(); }
	
	public static void add(){
		
		Person person;
		String input = null;
		System.out.println("Hej där, lägga till lite personer i listan? (ja/nej)");
		input = scanner.next();
		
		if(input.equalsIgnoreCase("ja") == true){
			
			do{
				person = new Person();
				
				System.out.println("Förnamn : ");
				input = scanner.next();
				if(input.equalsIgnoreCase("stop")) return;
				person.setfName(input);
				
				System.out.println("Efternamn : ");
				input = scanner.next();
				if(input.equalsIgnoreCase("stop")) return;
				person.setLName(input);
				
				System.out.println("Email : ");
				
					do{
						input = scanner.next();
						if(input.equalsIgnoreCase("stop")) return;
						if(Validate.validateEmail(input)){
							person.setMail(input);
							break;
						}else{
							System.out.println("Inte en giltig email, prova igen.");
						}
					}while(!Validate.validateEmail(input));
				 
				 System.out.println("Mobil : ");
					
					do{
						input = scanner.next();
						if(input.equalsIgnoreCase("stop")) return;
						if(Validate.validatePhone(input)){
							person.setPhone(input);
							break;
						}else{
							System.out.println("Inte ett giltigt telefonnummer, prova igen.");
						}
					}while(!Validate.validatePhone(input) );
					
					People.addPerson(person);
					show();
			}while(true);
		}else {
			return;
		}
		
	}
	
	public static void show(){
		if(!(People.getPeopleCount() == 0)){
			People.showPeople();
		}else{
			System.out.println(" Du har inte några personer att visa.");
		}
		
	}
	
	public static void exit(){
		scanner.close();
		scanner = null;
		System.exit(1);
	}

}
