package uppgift;

import java.util.regex.Pattern;


public class Validate {
	
	static String className = "Validate";
	static String fileName = "Validate.java";

	
	// Constructor
	public Validate() {
		
	}
	/**
	 * Validates a date with format %y%y%y%y .-/ %m%m .-/ %d%d
	 * @param s
	 * @return
	 */
	static boolean validateDate(String s){
		String regex = "([0-9]{4}\\./-\\s\\+[0-9]{2}\\./-\\s\\+[0-9]{2})";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
	}
	// Check and return true if a string is formatted correct as an email.
	static boolean validateEmail(String s){
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
	}
	
	// Validate if string is a phonenumber.
	static boolean validatePhone(String s){
		String regex = "([-?\\d+(\\.\\d+\\s]{5,})?";
		//String pattern = "^[+\\d+]{3,}+([\\d+\\-.\\s]{2,})+([\\d+\\s\\-.]{5,})";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
	}
	/**
	 *  Validates a birthdate. 
	 * @param s
	 * @return boolean
	 */
	static boolean validateBD(String s){
		String regex = "^([\\d+]{6,})+([-\\s])+([\\d+]{4,})$";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
	}

}
