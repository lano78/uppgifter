package uppgift;

public class Join {
	
	static String className = "Join";
	static String fileName = "Join.java";
	
	private static StringBuilder cache = new StringBuilder();
	

	// Constructor
	public Join() {
		
	}
	
	static void addToCache(String str){
		cache.append(str);
	}

	static StringBuilder getCache(){
		return cache;
	}
	
	static void clearCache(){
		cache.delete(0, cache.length());
	}
	
	// Join two strings into one separated by a space
	static String joinPartsWithSpaceSeparator(String a, String b){
		return a + " " + b;
	}
	// Join two strings into one separated by a comma.
	static String joinPartsWithCommaSeparator(String a, String b){
		return a + "," + b;
	}
	
}
