package uppgift;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ConnectionHandler {

	private String DB_NAME;
	
	private String USER_NAME;
	private String USER_PASSWORD;
	private String DB_CLASS_NAME = "com.mysql.jdbc.Driver";
	private String DB_CONN = "jdbc:mysql://localhost/";
	
	private static ConnectionHandler instance = null;
	
	protected ConnectionHandler(String dbName, String username, String password) {
		this.DB_NAME = dbName;
		this.USER_NAME = username;
		this.USER_PASSWORD = password;
	}
	protected ConnectionHandler(){
		System.out.println(" => Create an instance of ConnectionHandler with no args.");
	}
	
	/**
	 * 
	 * @return instance:ConnectionHandler
	 */
	public static ConnectionHandler getInstance(){
		if(instance == null){
			instance =  new ConnectionHandler();
		}
		return instance;
	}
	
	/**
	 * 
	 * @param dbName:String
	 * @param username:String
	 * @param password:String
	 */
	public void createNewDatabaseIfNotExists(String dbName, String username, String password){
		
		String query = "CREATE DATABASE IF NOT EXISTS " + dbName + ";";
		
		String CLASS_NAME = "com.mysql.jdbc.Driver";
		String URL = "jdbc:mysql://localhost/";
		
		String USER_NAME = username;
		String PASSWORD = password;
		
		Connection connection = null;
		Statement statement = null;
		
		try{
			// Register the class.
			Class.forName(CLASS_NAME);
			// Create a connection.
			connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
			// Create a statement.
			statement = connection.createStatement();
			
			// Execute the query.
			statement.executeUpdate(query);
			
			// Let us know if we succeded.
			System.out.println(" Created database : " + dbName);
			
		}catch(ClassNotFoundException | SQLException exception){
			exception.printStackTrace();
		}finally{
			// Close statement if not closed
		      try{
		         if(statement != null)
		        	 statement.close();
		      }catch(SQLException se){
		      
		      }
		      // Close connection if not closed.
		      try{
		         if(connection != null )
		      
		        	 connection.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		}
	}
	
	/**
	 * Drop a database if exists.
	 * @param dbName
	 * @param username
	 * @param password
	 */
	public void dropDatabaseIfExists(String dbName, String username, String password){
		String query = "DROP DATABASE IF EXISTS " + dbName + ";";
		
		String CLASS_NAME = "com.mysql.jdbc.Driver";
		String URL = "jdbc:mysql://localhost/";
		
		String USER_NAME = username;
		String PASSWORD = password;
		
		Connection connection = null;
		Statement statement = null;
		
		try{
			// Register the class.
			Class.forName(CLASS_NAME);
			// Create a connection.
			connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
			// Create a statement.
			statement = connection.createStatement();
			
			// Execute the query.
			statement.executeUpdate(query);
			
			// Let us know if we succeded.
			System.out.println(" Created database : " + dbName);
			
		}catch(ClassNotFoundException | SQLException exception){
			exception.printStackTrace();
		}finally{
			// Close statement if not closed
		      try{
		         if(statement != null)
		        	 statement.close();
		      }catch(SQLException se){
		      
		      }
		      // Close connection if not closed.
		      try{
		         if(connection != null )
		      
		        	 connection.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		}
	}

	//========================================================
	// QueryMethods
	//========================================================
	
	/**
	 * Query a the database by id and return an instance of Person class.
	 * @param id
	 * @return Person instance
	 */
	public Person getPersonWithId(int id){
		
		String DB_NAME = "personer";
		String USER_NAME = "root";
		String USER_PASSWORD = "";
		String DB_CLASS_NAME = "com.mysql.jdbc.Driver";
		String DB_CONN = "jdbc:mysql://localhost/" + DB_NAME;
		String TABLE_NAME = "elever";
		
		//
		String query = "SELECT fname, lname, email, phone FROM " + TABLE_NAME + " WHERE id='" + id + "';";
		
		//
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		//
		String fname = null;
		String lname = null;
		String email = null;
		String phone = null;
		
				try{
					//
					connection = DriverManager.getConnection(DB_CONN, USER_NAME, USER_PASSWORD);
					//
					Class.forName(DB_CLASS_NAME);
					//
					statement = connection.createStatement();
					//
					resultSet = statement.executeQuery(query);
					//
					while(resultSet.next()){
						
							 fname = resultSet.getString("fname");
							 lname = resultSet.getString("lname");
							 email = resultSet.getString("email");
							 phone = resultSet.getString("phone");
						
					}
					
				}catch(ClassNotFoundException | SQLException exception){
					exception.printStackTrace();
				}finally{
				      try{
					      if(statement != null)
					        	 statement = null;
					        	 connection.close();
					      }catch(SQLException e){
					    	  e.printStackTrace();
					      }// do nothing
					      try{
					         if(connection != null )
					        	 statement = null;
					        	 connection.close();
					      }catch(SQLException e){
					         e.printStackTrace();
					      }
					
				}
		
		return new Person(fname, lname, email, phone);
	}
	
	
	
	
	/**
	 * getColumnNames(String table)
	 * @param table - takes a table from the db.
	 * @return Array of column names and print out the contents.
	 */
	public ArrayList<String> getColumnNames(String table){
		ArrayList<String> colNames = new ArrayList<String>();
		
		String query = "SELECT * FROM " + table;
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		ResultSetMetaData metaData = null;
		
		try{
				
				// 
				statement = connection.createStatement();
				//
				resultSet = statement.executeQuery(query);
				
				// Get metadata from table
				metaData = resultSet.getMetaData();
		
				// index for the array
				int idx = 1;
				for(String s : colNames){
					
					System.out.println(metaData.getColumnName(idx));
					colNames.add(metaData.getColumnName(idx) );
					idx++;
				}
			
					resultSet.close();
					statement.close();
		}catch(SQLException exception){
				exception.printStackTrace();
		}finally{
			try {
				resultSet.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				statement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		return colNames;
	}
	

	/** 
	 * Takes a String and print the results to console.
	 * @param query takes a string.
	 */
	public void queryDB(String s){
		
		Connection connection = null;
		Statement statement = null;
		
		try{
			

			// 
			connection = DriverManager.getConnection(DB_CONN, USER_NAME, USER_PASSWORD);
			//Class.forName(DB_CLASS_NAME);
			statement = connection.createStatement();
			//
			ResultSet result = statement.executeQuery(s);
			
			// Get metadata from table
			ResultSetMetaData meta = result.getMetaData();
			
			// Get num columns in db
			int colCnt = meta.getColumnCount();
			
					
			// Print the result from the query.
			while(result.next()){
				// loop the columns

				System.out.println();
				for (int i = 1; i <= colCnt; i++ ){
					Object value = (Object) result.getObject(i);
					System.out.print(value);
					System.out.print("\t");
				}
				System.out.println();
			}
			result.close();
			statement.close();
		}catch(SQLException exception){
			exception.printStackTrace();
		}
		System.out.println();
		//closeDB();	
	}
	/**
	 * Inserts a Person from the person arrayList
	 * @param peopleArray
	 * @param index
	 */
	public void insertToDb(ArrayList<Person> p, int i){
		
		Statement stmt = null;
		Connection conn = null;
		Person person = p.get(i);
		
		String query = "INSERT INTO elever(fname, lname, email, phone)"
				+ "VALUES('"
				+ person.getfName() + "','"
				+ person.getLName() + "','"
				+ person.getMail() + "','"
				+ person.getPhone() + "');";
		try{
			
			Class.forName(DB_CLASS_NAME);
			conn = DriverManager.getConnection(DB_CONN, USER_NAME, USER_PASSWORD);
		
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			
		}catch(ClassNotFoundException | SQLException exception){
			exception.printStackTrace();
		}finally{
			 //finally block used to close resources
		      try{
		         if(stmt != null)
		        	 //stmt = null;
		            conn.close();
		      }catch(SQLException se){
		      
		      }// do nothing
		      try{
		         if(conn != null )
		        	 //Sstmt = null;
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		}
	}
	/**
	 * Inserts a class instance as blob into blobs table.
	 * @param arrayList
	 * @param index
	 */
	public void insertBlob(ArrayList<Person> p, int i){
		
		Connection connection = null;
		Statement statement = null;
		Person person = p.get(i);
		String query = "INSERT INTO blobs(object)"
				+ "VALUES('" + person + "');";
		try{
			Class.forName(DB_CLASS_NAME);
			connection = DriverManager.getConnection(DB_CONN, USER_NAME, USER_PASSWORD);
		
			statement = connection.createStatement();
			statement.executeUpdate(query);
			
		}catch(ClassNotFoundException | SQLException exception){
			exception.printStackTrace();
		}finally{
			 //finally block used to close resources
		      try{
		         if(statement != null)
		        	 //stmt = null;
		        	 statement.close();
		      }catch(SQLException se){
		      
		      }// do nothing
		      try{
		         if(statement != null )
		        	 //Sstmt = null;
		        	 statement.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		}
	}
	

}

