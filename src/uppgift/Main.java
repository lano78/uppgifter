package uppgift;

import java.util.Scanner;



public class Main {
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println();
		
		// Start the program.
		//LogicHandler.start();
		
		ConnectionHandler conn = new ConnectionHandler();
		
		Person person = conn.getPersonWithId(1);
		
		System.out.println(person.getfName() + ", " + person.getLName() + ", " + person.getMail() + ", " + person.getPhone());
		
		System.out.println(Validate.validateDate("1978-10-04"));
		
	}

}
