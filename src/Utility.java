import java.io.File;
import java.io.IOException;

public class Utility {
	
	
	
	public void loadTextFile(String path, String fileName){
			 
	}
	
	public long elapsedTimeMs(long start, long stop){
		System.out.println("==> Elapsed Time : " + (stop - start) );
		return (stop - start);
	}
	
	//Check if an int is a prime.
	public boolean isPrime(int num){
	    
		//check if n is a multiple of 2
	    if (num % 2 == 0) return false;
	    
	    //if not, then just check the odds
	    for(int i = 3; i * i <= num; i += 2 ) {
	        
	    	if( num % i == 0) return false;
	    }
	    return true;
	}
	
	// Distance between two locations( Equirectangular approximation )
	public double distanceApproximation(double lat1, double long1, double lat2, double long2){

		int EARTH_RADIUS_IN_METERS = 6378137;
		
		double l1   = Math.toRadians(lat1); 
		double lng1 = Math.toRadians(long1);
		double l2   = Math.toRadians(lat2); 
		double lng2 = Math.toRadians(long2);
		
		double x = (lng2 - lng1) * Math.cos( (l1 + l2) * 0.5 );
		double y = (l2-l1);
		
		return ( Math.sqrt( (x * x) + ( y * y ) ) * EARTH_RADIUS_IN_METERS );
	}
	
}











