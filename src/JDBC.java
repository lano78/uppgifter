//-------------------------------------------------------
// JDBC test.
// YA - java HT 2014
// 
//-------------------------------------------------------

//import Libs.
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import JDBC.InputHandler;

public class JDBC{
	// use the inputhandler.
	static InputHandler inputHandler = new InputHandler();
	// variables
	private final static String dbName = "EmployeeDB";
	private final static String dbConn = "jdbc:mysql://127.0.0.1/" + dbName;
	
	// Variables 
	private final static String USERNAME = "root";
	private final static String PASSWORD = "";
	
	// Connection..
	static Connection connection = null;
	
	// Properties class 
	static Properties property = new Properties();
	
	public static void main(String[] args) {
	
		String input = inputHandler.inputResult();
		System.out.println(input);
		// My username and password to the server.
		property.put("user", USERNAME);
		property.put("password", PASSWORD);
		
		// try connect to my database
		try {
			
			// Not use the property..
			connection = DriverManager.getConnection(dbConn, USERNAME, PASSWORD);
			
			// Use property
			//connection = DriverManager.getConnection(dbConn, property);
			
			System.out.println(" ===>> Connected to Database was a success!!");
		
			//Did something go wrong, tell me...
		} catch (SQLException exception) {
			
			exception.printStackTrace();
			System.out.println(" ===>> No connection to " + dbName + ", something went wrong... ");
		
			// ok, we are done with the database.
		}finally{
			try{
				
				// check if connection is open, close if so.
				if(connection != null)
					connection.close();
				System.out.println(" ===>> Connection closed.");
				// ok, what went wrong...
			}catch(SQLException exception){
				exception.printStackTrace();
				System.out.println(" ===>> OK, what went wrong..?!?");
			}
		}
	}

}
