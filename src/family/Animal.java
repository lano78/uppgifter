package family;

public class Animal extends Object {
	
	private String ofKind;
	private String role;
	private String name;
	private String gender;
	private int age;
	private int numLegs;
	private String owner;
	
	
	public Animal() {
		this.ofKind = setKind("Animal");
		this.setRole("Dog");
		this.setName("Fido");
		this.setGender("Male");
		this.setAge(10);
		this.setNumLegs(4);
		this.setOwner("Hugo");
	}

	public String getOfKind() {
		return ofKind;
	}

	public void setOfKind(String ofKind) {
		this.ofKind = ofKind;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public int getNumLegs() {
		return numLegs;
	}

	public void setNumLegs(int numLegs) {
		this.numLegs = numLegs;
	}
	
	public void show(){
		System.out.println("\t" + this.role);
		System.out.println("---------------------");
		System.out.println(" Kind\t: " + this.ofKind);
		System.out.println(" Name\t: " + this.name);
		System.out.println(" Gender\t: " + this.gender);
		System.out.println(" Age\t: " + this.age);
		System.out.println(" Role\t: " + this.role);
		System.out.println(" Legs\t: " + this.numLegs);
		System.out.println("---------------------");
	}


}
