package family;

public class Home extends Object {
	
	private String ofKind;
	private String ofType;
	private int numWalls;
	private boolean hasBalcony;
	private String owner;
	
	public Home() {
		this.ofKind = setKind("Building");
		this.setOfType("House");
		this.setNumWalls(4);
		this.setHasBalcony(true);
		this.setOwner("Lars");
		
	}

	public String getOfKind() {
		return ofKind;
	}

	public void setOfKind(String ofKind) {
		this.ofKind = ofKind;
	}

	public String getOfType() {
		return ofType;
	}

	public void setOfType(String ofType) {
		this.ofType = ofType;
	}

	public int getNumWalls() {
		return numWalls;
	}

	public void setNumWalls(int numWalls) {
		this.numWalls = numWalls;
	}

	public boolean getHasBalcony() {
		return hasBalcony;
	}

	public void setHasBalcony(boolean hasBalcony) {
		this.hasBalcony = hasBalcony;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public void show(){
		System.out.println("\t" + this.ofKind);
		System.out.println("---------------------");
		System.out.println(" Type\t: " + this.ofType);
		System.out.println(" Owner\t: " + this.owner);
		System.out.println(" Walls\t: " + this.numWalls);
		System.out.println(" Balcony: " + this.hasBalcony);
		System.out.println("---------------------");
	}


}
