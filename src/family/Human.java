package family;

public class Human extends Object {
	
	private String ofKind;
	private String gender;
	private String name;
	private int age;
	private String role;
	private String ofType;
	
	
	public Human() {
		this.ofKind = setKind("Human");
		this.setAge(36);
		this.setGender("Male");
		this.setName("Lars");
		this.setRole("Father");
		this.setOfType(null); 
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOfKind() {
		return ofKind;
	}

	public void setOfKind(String ofKind) {
		this.ofKind = ofKind;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getOfType() {
		return ofType;
	}

	public void setOfType(String ofType) {
		this.ofType = ofType;
	}
	
	public void show(){
		System.out.println("\t" + this.role );
		System.out.println("---------------------");
		System.out.println(" Kind\t: " + this.ofKind);
		System.out.println(" Name\t: " + this.name);
		System.out.println(" Gender\t: " + this.gender);
		System.out.println(" Age\t: " + this.age);
		System.out.println(" Role\t: " + this.role);
		System.out.println(" Type\t: " + this.ofType);
		System.out.println("---------------------");
	}


}
