package family;

public class Main {
	
	static Human human = new Human();
	static Animal dog = new Animal();
	static Home house = new Home();
	
	static Human child = new Human();
	
	public static void main(String[] args) {
		// print the predefined values of the human class.
		human.show();
		
		// overrides predefined values.
		human.setName("Hugo");
		human.setAge(6);
		human.setRole("Child");
		human.setOfType("Son");
		
		// print the new values for the human instance.
		human.show();
		
		// print the predefined values of the dog class
		dog.show();
		
		// override the predefined values of the dog instance.
		dog.setName("Blixten");
		dog.setAge(4);
		dog.setRole("Cat");
		
		// print the new values for the dog instance.
		dog.show();
		
		
		// print the predefined valuse for the house class.
		house.show();
		
		// override the predefined valuse for the house instance.
		house.setOfType("Office");
		house.setOwner("Hugo Inc");
		house.setNumWalls(6);
		house.setHasBalcony(false);
		
		// print the new valuse of the house instance.
		house.show();
		
		//
		System.out.println(" " + dog.getName() + "s owner is " + dog.getOwner());
		
		// 
		
		
	}

}
