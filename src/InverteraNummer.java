//---------------------------------------------
// InverteraNummer.java
// Räknar upp och räknar ner från input.
//---------------------------------------------
import java.util.Scanner;
public class InverteraNummer {
	
	public static void main(String[] args) {
		System.out.println("V�lj ett nummer mellan 1-20: ");
		
		Scanner scanner = new Scanner(System.in);
		int myNum = scanner.nextInt();
		int count;
		
		//while( (count <= 20) ){}
		for( count = myNum; count <= 20; count++ ){
			System.out.println(count);
		}
		
		for(int i = count; i >= 1; --i ){
			count = i;
			System.out.println(count);
		}
		
		scanner.close();
	}
	
}
