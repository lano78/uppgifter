//---------------------------------------------
// TestClass.java
// Class for att testa olika saker.
//---------------------------------------------
import java.util.*;


public class TestClass implements SecretSauce {
	
	static Utility ut = new Utility();
	static long start = System.currentTimeMillis();
	
	
	public static void main(String[] args) {
		System.out.println("start " + System.currentTimeMillis() );
		long stop = System.currentTimeMillis();
		long et = ut.elapsedTimeMs(start, stop);
		// Init the scanner so we can listen for user input.
		Scanner input = new Scanner(System.in);
		System.out.println("Ange nummer " );
		int num = input.nextInt();
		System.out.println(num + " is  prime = " + ut.isPrime(num) );
		System.out.println("eT " + et );
		
		System.out.println(" Dist = " + ut.distanceApproximation(63.17219, 14.64795, 63.17211, 14.64446) + " m" );

	}

}
