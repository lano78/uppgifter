//---------------------------------------------
// NumberModulo.java
// Kollar om nummret är jämnt delbart.
//---------------------------------------------

import java.util.Scanner;

public class NumberModulo {
	
	public static void println(String line){ 
		System.out.println(line);
		}
	
	public static void main(String[] args) {
		boolean correct = true;
		Scanner scanner = new Scanner(System.in);
		System.out.println(" Nummer 1-20");
		while(correct){
			int myNum = scanner.nextInt();
			if ( (myNum%3 == 0) || (myNum%5 == 0) ) {
				System.out.println("Rätt!");
				correct = false;
			} else{
				System.out.println("Super Fel!");
				
			}
			
			System.out.println();
		}

		
		scanner.close();
	}
}
