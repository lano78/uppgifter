package oop;

public class Calculate {
	
	private double width;
	private double height;
	private double depth;
	
	private double volume;
	
	/**
	 * 
	 * @param w
	 * @param h
	 * @param d
	 */
	
	public Calculate(double w, double h, double d) {
			System.out.println(" Using three arg Constructor");
			this.width = w;
			this.height = h;
			this.depth = d;
	}
	public Calculate(double w, double h) {
			System.out.println(" Using two arg Constructor, depth is set to 0");
			this.width = w;
			this.height = h;
			this.depth = 0;
	}
	public Calculate(double w) {
			System.out.println(" Using one arg Constructor, depth is set to 0, height is set to 0");
			this.width = w;
			this.height = 0;
			this.depth = 0;
	}
	public Calculate() {
			System.out.println(" Using zero arg Constructor, all args set to 0");
			this.width = 0;
			this.height = 0;
			this.depth = 0;
	}
	
	// Get the width of the variables value.
	public double getWidth() {
		return width;
	}
	// Set the width vaiable.
	public void setWidth(double width) {
		this.width = width;
	}
	// Get the height variables value.
	public double getHeight() {
		return height;
	}
	// Set the height vaiable
	public void setHeight(double height) {
		this.height = height;
	}
	// Get the depth variables value.
	public double getDepth() {
		return depth;
	}
	// Set the depth variable
	public void setDepth(double depth) {
		this.depth = depth;
	}
	// Get the volume variables value.
	public double getVolume() {
		return volume;
	}
	
	// Set the volume variable.
	public void setVolume(double volume) {
		this.volume = volume;
	}
	
	// Calculate the volume of a box
	public double calculateVolume(double w, double h, double d){ 
		return (w * h * d);
	}

}




