package oop;

import java.util.Scanner;


public class Main {
	// Create a Scanner instance.
	static Scanner scanner = new Scanner(System.in);
	
	// Forward declaration of scanner input.
	static double input;
	
	public static void main(String[] args) {
		System.out.println(" Hallå där lådan!");
		
		// Create a Box instance with width = 0, height = 0, depth = 0
		Calculate box = new Calculate(1,1,1);
		
		// Enter width, height and depth so we can calculate the volume later.
		System.out.println(" Vad är din bredd?");
		
		// get the width input as a double 
		input = scanner.nextDouble();
		
		// Set the width variable for the box instance.
		box.setWidth(input);
		
		System.out.println(" Vad är din höjd?");
		
		// get the height input as a double 
		input = scanner.nextDouble();
		
		// Set the height variable for the box instance.
		box.setHeight(input);
		
		System.out.println(" Vad är ditt djup?");
		
		// get the depth input as a double 
		input = scanner.nextDouble();
		
		// Set the depth variable for the box instance.
		box.setDepth(input);
		
		// Calculate the volume of the box instance.
		box.setVolume( box.calculateVolume(box.getWidth(), box.getHeight(), box.getDepth()) );
		
		// Output the box instance volume.
		System.out.println(" Din volym är " + box.getVolume() + "m3");
		
		// Close the scanner to avoid a memoryleak.
		scanner.close();
	}

}
