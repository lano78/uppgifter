/**
 * 
 */
package JDBC;
/**
 * Project : JDBC OOP.
 * YA - java HT 2014.
 * @author larsnordstrom
 * 
 */
import java.sql.*;
import java.util.Properties;


public class SqlHandler extends InputHandler implements Constants, Queries  {
	
	private static final String dbClassName = "com.mysql.jdbc.Driver";
	
	Connection connection = null;
	Statement stmt = null;
	
	Properties property = new Properties();
	
	/**
	* Create a database.
	* @param name of database.
	*/
	public void createDB(String name){
		try{
			stmt = connection.createStatement();
			
			String sql = CREATE_DB + name;
			stmt.executeUpdate(sql);
			System.out.println(" ===>>> Created Database with name : " + name);
		}catch(SQLException exception){
			exception.printStackTrace();
		}
	}
	
	
	/**
	 * Create a user at the current database.
	 * @param username string. 
	 * @param ip string, your computer ip.
	 * @param password string.
	 */
	public void createUser(String username, String ip, String password){
		try{
			// Check if db is open, open if not.
			if ( !checkConn() ){
				openDB();
			}
			
			Statement stmt = connection.createStatement();
			
			String sql = CREATE_USER + "'" + username + "'@'" + ip + ID + "'" + password + "';";
			
			stmt.executeUpdate(sql);
			
		}catch(SQLException exception){
			exception.printStackTrace();
		}
		
	}
	/** 
	 * Takes a String and print the results to console.
	 * @param query takes a string.
	 */
	public void queryDB(String query){
		try{
			// Check if db is open, open if not.
			if ( !checkConn() ){
				openDB();
			}
			// 
			Statement stmt = connection.createStatement();
			//
			ResultSet result = stmt.executeQuery(query);
			
			// Get metadata from table
			ResultSetMetaData meta = result.getMetaData();
			
			// Get num columns in db
			int colCnt = meta.getColumnCount();
			
					
			// Print the result from the query.
			while(result.next()){
				// loop the columns
				System.out.println();
				for (int i = 1; i <= colCnt; i++ ){
					Object value = result.getObject(i);
					System.out.print(value);
					System.out.print("\t");
				}
				System.out.println();
			}
			result.close();
			stmt.close();
		}catch(SQLException exception){
			exception.printStackTrace();
		}
		System.out.println();
		closeDB();	
	}
	
	/**
	 * 
	 * @param pathToFile filepath to csv.
	 * @param toTable what table data should go.
	 */
	public void loadTableFromCSV(String pathToFile, String toTable){
		try{
			// Check if db is open, open if not.
			if ( !checkConn() ){
				openDB();
			}
			//System.out.println(" ===>>> Create Database with name : " + name);
			stmt = connection.createStatement();
			
			String sql = "load data local infile '" + pathToFile + "' into table " + toTable + " fields terminated by ',' lines terminated by '\n' ";// ignore 1 rows;";
			stmt.executeUpdate(sql);
		}catch(SQLException exception){
			exception.printStackTrace();
		}
		closeDB();	
	}

	//=================================================================
	//==
	//=================================================================
	public static String getDbclassname() {
		return dbClassName;
	}
	
	/**
	 * Connects to a database 
	 */
	public void connect(){
		// Check if db is open, open if not.
		if ( !checkConn() ){
			openDB();
		}

	}
	
	//=================================================================
	//== Private methods
	//=================================================================

	/**
	 * Private method.
	 * Check if there is an db connection
	 * @return 
	 * true if there is a connection to a database otherwise return false.
	 */
	private boolean checkConn(){

		try{
			if(connection != null){
				System.out.println(" ===>> Connection to database OK ( " + DB_NAME + " )");
				return true;
			}else{
				System.out.println(" ===>> Connection to database Error.");
				return false;
			}
		}finally{
			
		}
	}

	
	// initiate connection to db.
	/**
	 * Opens a database
	 */
	private void openDB(){

		// if not remote, use local user info.
		String username = LOCAL_USERNAME;
		String password = LOCAL_PASSWORD;
		String db_conn  = LOCAL_DB_CONNECTION;
		
		// if remote, use remote user info.
		if (IS_REMOTE_DB) {
			username = REMOTE_USERNAME;
			password = REMOTE_PASSWORD;
			db_conn  = REMOTE_DB_CONNECTION;
			
		}
		// Not used...
		// property.put("user", username);
		// property.put("password", password);
		// connection = DriverManager.getConnection(db_conn, property);
		try {
			Class.forName(dbClassName);
			connection = DriverManager.getConnection(db_conn, username, password);
			System.out.println(" ===>> Connected to database :" + DB_NAME);
		} catch (ClassNotFoundException | SQLException exception) {
			
			exception.printStackTrace();
		}
	}
	
	// Close the DB if it is open.
	/**
	 * Close the current database if open.
	 */
	private void closeDB(){
		try{
			// Check if db is open.
			if ( checkConn() ){
				connection.close();
				System.out.println(" ===>> Disconnected from database :  " + DB_NAME);
			}
			
		}catch(SQLException exception){
			exception.printStackTrace();
		}
	}

	
		
}












