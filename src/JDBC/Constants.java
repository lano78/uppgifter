/**
 * 
 */
package JDBC;
/**
 * Project : JDBC OOP.
 * YA - java HT 2014.
 * @author larsnordstrom
 * 
 */

public interface Constants {

	public final boolean IS_REMOTE_DB = false; 
	// local login
	public final String LOCAL_USERNAME = "root";
	public final String LOCAL_PASSWORD = "";
	
	// Local connection
	public final String DB_NAME = "statistics";
	public final String LOCAL_URL = "127.0.0.1";
	
	// Remote Connection
	public final String MY_IP = "192.168.1.67";
	public final String REMOTE_USERNAME = "lano78";
	public final String REMOTE_PASSWORD = "6859Hugo";
	public final String SERVER_IP = "192.168.1.87";
	public final String PORT = "3306";
	
	// Local mysql db
	public final String LOCAL_DB_CONNECTION = "jdbc:mysql://" + LOCAL_URL + "/" + DB_NAME;
	
	// Shared mysql on local network.
	public final String REMOTE_DB_CONNECTION = "jdbc:mysql://" + MY_IP + ":" + PORT + "/" + DB_NAME + "?user=" + REMOTE_USERNAME + "&password=" + REMOTE_PASSWORD;


}




