
package NamnListan;

public class Person {
	private String fNAME;
	private String eNAME;
	private int AGE;
	private int mPHONE;
	
	public String getFirstName(){
		return fNAME;
	}
	
	public String getLastName(){
		return eNAME;
	}
	
	public int getAge(){
		return AGE;
	}
	
	public int getPhone(){
		return mPHONE;
	}
	
	public void setFirstName(String val){
		this.fNAME = val;
	}
	
	public void setLastName(String val){
		this.eNAME = val;
	}
	
	public void setAge(int val){
		this.AGE = val;
	}
	
	public void  setPhone(int val){
		this.mPHONE = val;
	}
	
	public void resetValues(){
		this.fNAME  = null;
		this.eNAME  = null;
		this.AGE    = 0;
		this.mPHONE = 0;
	}
	
}
