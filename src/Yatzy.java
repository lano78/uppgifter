//--------------------------------------------------------------------------------
// Todo;
// find combos, add to score
// GUI
//--------------------------------------------------------------------------------

import java.util.Random;
import java.util.Scanner;
//import java.util.Arrays;
//import java.util.List;
//import java.util.ArrayList;

public class Yatzy implements YatzyConstants {
	
	public static void main(String[] args) {
		
		// Seed for better random.
		long seed = System.currentTimeMillis();
		Random mRand = new Random(seed); 
		
		// print the seed, just for fun...
		System.out.println("Seed " + seed);
		System.out.println("IS MAC " + IS_MAC);
		System.out.println("DIR : " + CURRENT_DIR);
		// Init the scanner so we can listen for user input.
		Scanner input = new Scanner(System.in);
		
		// dice have num sides. set it to final because a dice only have 6 sides.
		final int diceSides = 6;
		
		// Array for the dice rolls.
		int [] diceRolls = new int[5];
		
		// Array for the sorted rolls, declare
		int [] sortedArray;
		// What can we get from out sortedArray? (pair, 2pair...etc)
		String comboType = "";
		
		// score for that combo.
		int score = 0;
		
		// just for easy math.
		int diceRollLen = (diceRolls.length - 1);
		
		// Max turns to play, number of turns played, turns we rolled our dice.
		final int maxTurns = 15;
		int turnCount = 0;
		int rollCount = 0;
		// Bonus
		final int bonusValue = 50;
		boolean checkForBonus = true;
		boolean earnedBonus = false;
		
		// Array for the kinds of scores
		String [] scoreTypes = {"Ettor", "Tvåor", "Treor", "Fyror", "Femmor", "Sexor", "Summa", "Bonus", "1 Par", "2 Par", "Triss", "Fyrtal", "Liten", "Stor", "Chans", "Yatzy", "Totalt"};
		
		// Array for keeping track of what slots have been used, true = used, false = empty.
		boolean [] usedTypes = new boolean[scoreTypes.length];

		// Array for score input.
		int [] scoreArray = new int[scoreTypes.length];
		
		//--------------------------------------------------------------------------------
		// Game states
		//--------------------------------------------------------------------------------
		boolean isRunning = true;
		boolean isGameOver = false;
		//--------------------------------------------------------------------------------
		// Menu
		//--------------------------------------------------------------------------------
		String [] menuOptions = {"r", "s", "e" };
		String [] menuDescriptions = { "Slå tärningarna.", "Visa Poängen.",  "Avsluta Spelet." };
		String [] messages = { "Tryck 'r' för att slå tärningarna.", "Game Over, Alla rundor spelade!", "Du fick bonuspoäng!", "Summa poäng", "Avslutade spelet.", "Spela en ny match ( y/n)?", "Starta om spelet" };
		
		// Print the menu options.
		System.out.println("---------- Meny -----------");
		for (int i = 0; ( i <= (menuOptions.length - 1) ); i++){
			System.out.println("-- " + menuOptions[i] + " = " + menuDescriptions[i]);
		}
		
		//--------------------------------------------------------------------------------
		// Game loop. 
		//--------------------------------------------------------------------------------
		do{
			isRunning = false;
			if (!isRunning){
				// Press "r" to start the game.
				System.out.println( messages[0]);
				
				// our userInput for what to do.
				String userInput = input.next();
				
				// We did press "r" so roll the dices.
				if ( userInput.equals("r") ) {
					
					// Roll all 5 dices, random number 1-6
					for (int i = 0; ( i <= diceRollLen); i++){
						// insert the dice values in the diceRolls Array for later use.
						diceRolls[i] = mRand.nextInt( diceSides ) + 1;
						System.out.println(diceRolls[i]);
					}
					turnCount++;
					
					System.out.println("Turn Count " + turnCount);
					
					//---------------------------------------------------------------
					// Sort the dices. How many of each kind?
					//---------------------------------------------------------------
					
					// We init the sortedArray here so it gets cleared each round. 
					sortedArray = new int[diceSides];
					
					for (int i = 0; i <= diceRollLen; i++){
						// ones
						if (diceRolls[i] == 1){
							sortedArray[0]++;
						}
						// twos
						else if (diceRolls[i] == 2){
							sortedArray[1]++;
						}
						// threes
						else if (diceRolls[i] == 3){
							sortedArray[2]++;
						}
						// fours
						else if (diceRolls[i] == 4){
							sortedArray[3]++;
						}
						// fives
						else if (diceRolls[i] == 5){
							sortedArray[4]++;
						}
						// sixes
						else if (diceRolls[i] == 6){
							sortedArray[5]++;
						}
						
					}
					System.out.println("-----------------");
					System.out.println("Ettor  " + sortedArray[0]);
					System.out.println("Tvåor  " + sortedArray[1]);
					System.out.println("Treor  " + sortedArray[2]);
					System.out.println("Fyror  " + sortedArray[3]);
					System.out.println("Femmor " + sortedArray[4]);
					System.out.println("Sexor  " + sortedArray[5]);
					System.out.println("-----------------");
					
					//---------------------------------------------------------------
					// What did we get? ( 1pair, 2pair...etc)
					//---------------------------------------------------------------
					
					// Singles
					
					// pairs
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						// 
						int scoreMultiplier = (i + 1);
						if (sortedArray[i] == 2) {
							score = (2*scoreMultiplier);
							comboType = scoreTypes[8];
							System.out.println(comboType + " i " + scoreMultiplier);
							break;
						}
					}
					// 2 pairs
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						// 
						int scoreMultiplier = (i + 1);
						if (sortedArray[i] == 2) {
							score = (2*scoreMultiplier);
							if (sortedArray[i] == 2) {
							score = score + (2*scoreMultiplier);
							comboType = scoreTypes[8];
								System.out.println(comboType + " i " + scoreMultiplier);
							break;
							}
						}
					}
					// threes
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						// 
						int scoreMultiplier = (i + 1);
						if (sortedArray[i] == 3) {
							score = (3*scoreMultiplier);
							comboType = scoreTypes[10];
							System.out.println(comboType + " i " + scoreMultiplier);
							break;
						}
					}
					// fours
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						// 
						int scoreMultiplier = (i + 1);
						if (sortedArray[i] == 4) {
							score = (4*scoreMultiplier);
							comboType = scoreTypes[11];
							System.out.println(comboType + " i " + scoreMultiplier);
							break;
						}
						
					}
					// small straight
					for (int i = 0; i <= (sortedArray.length - 2); i++ ){
						// 
						if (sortedArray[i] == 1) {
							score = 15;
							comboType = scoreTypes[12];
							break;
						}
						//System.out.println(comboType);
						
					}
					// big straight
					for (int i = 1; i <= (sortedArray.length - 1); i++ ){
						// 
						if (sortedArray[i] == 1) {
							score = 21;
							comboType = scoreTypes[13];
							break;
						}
						//System.out.println(comboType);
					}
					// full house
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						// 
						int scoreMultiplier = (i + 1);
						
						if (sortedArray[i] == 2){
							score = (2*scoreMultiplier);
							if ( sortedArray[i] == 3) {
								score = score + (3*scoreMultiplier);
								comboType = scoreTypes[14];
								System.out.println(comboType);
								break;
							}
						
						}
					}
					// yatzy
					for (int i = 0; i <= (sortedArray.length - 1); i++ ){
						if (sortedArray[i] == 5) {
							score = 50;
							comboType = scoreTypes[15];
						    System.out.println(comboType);
						    break;
						}
						
					}
				}
				//--------------------------------------------------------------------------------
				// Check if we can use the/any combo we found
				//--------------------------------------------------------------------------------


				//--------------------------------------------------------------------------------
				// Do we get bonus? check until checkForBonus == false;
				//--------------------------------------------------------------------------------
				//do{
					//Check if scoreArray slot 0-5 >= 50
					for (int i = 0; i >= diceRollLen; i++){
						int sum = 0;
						// calculate the values of 0-5 (ettor - sexor) 
						sum = sum + scoreArray[i];
						
						// Set the total sum
						scoreArray[6] = sum;
						
						// Did we reach 50pts?
						if (sum >= bonusValue){
							// yes we did
							earnedBonus = true;
							checkForBonus = false;
							// Give us bonus.
							scoreArray[7] = bonusValue;
							System.out.println("Bonus!!!" + scoreArray[7]);
						}
						// does nothing.
						if (earnedBonus){
							
						}
						
					}
				//}while(checkForBonus);
				
				// Set isRunning to true again so we can play another turn when evaluation is done.
				isRunning = true;
				
				// Check if we have played all turns.
				if (turnCount == maxTurns){
					isGameOver = true;
					isRunning = false;
					turnCount = 0;
					System.out.println( messages[1]);
					
					// Calculate the length of the scoreArray - our last slot(Total).
					int scoreSlots = (scoreArray.length - 2);

					for (int i = 0; i >= (scoreSlots - diceRollLen); i++){
						
						int sum = 0;
						
						// Add all scores to see out total, do not include slot 0-5 (ettor - sexor).
						sum = sum + scoreArray[i];
						
						System.out.println( messages[3] + sum);
						
						if (i == scoreArray.length){
							scoreArray[scoreArray.length] = sum;
						}
					}
					
				}
				
				//--------------------------------------------------------------------------------
				// Press s to see the current scores.
				//--------------------------------------------------------------------------------
				if ( userInput.equals("s") ){
					System.out.println("-------------------");
					System.out.println("  Protokoll");
					System.out.println("-------------------");
					System.out.println("Typ\t| Poäng");
					System.out.println("-------------------");
					
					// Loop through the scoreArray and print current values.
					for (int i = 0; i <= (scoreTypes.length - 1); i++ ){
						if ( (i == 6) || (i == 16) ){
							System.out.println("-------------------");
							System.out.println(scoreTypes[i] + "\t| " + scoreArray[i] );
							System.out.println("-------------------");
						}else if (i == 7 ){
								System.out.println(scoreTypes[i] + "\t| " + scoreArray[i] );
								System.out.println("-------------------");
						}
						else{
							System.out.println(scoreTypes[i] + "\t| " + scoreArray[i] );
						}
					}
				}
				//--------------------------------------------------------------------------------
				// We decide to quit the game while playing.
				//--------------------------------------------------------------------------------
				if ( userInput.equals("e") ) {
					// Exit the program
					System.out.println( messages[4]);
					System.exit(0);
				}

			}
			// loop until game state changes or we tell it to stop.	
		}while(isRunning);
		
		//--------------------------------------------------------------------------------
		// if We are done playing ( No more turns to play )
		//--------------------------------------------------------------------------------
		if (isGameOver == true){
			// Sum up the scores and print on screen.
			
			// ask for a new game
			System.out.println( messages[6]);
			
			// our userInput for what to do.
			String userInput = input.next();
			// We did press "r" so roll the dices.
			if ( userInput.equals("y") ) {
				// Reset the gameStates
				System.out.println( messages[4]);
				isRunning = true;
				isGameOver = false;
			}else if ( userInput.equals("n") ) {
				// Exit the program
				System.out.println(messages[4]);
				System.exit(0);
			}else{
				System.out.println("Fel val!");
				System.out.println( messages[4]);
				System.exit(0);
				
			}
			
		}
		
		//--------------------------------------------------------------------------------
		// Close the Scanner to avoid Memory leaks.
		//--------------------------------------------------------------------------------
		input.close();
	}

}




















