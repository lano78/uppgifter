//---------------------------------------------
// SecretSauce.java
// Globala variabler för flera program.
//
//---------------------------------------------

public interface SecretSauce extends Device {
	
	String CURRENT_DIR = System.getProperty("user.dir");
	
	// Output folder name.
	public static final String OUTPUT_FOLDER = "Output";
	
	// Output path for different platforms
	public static final String MAC_PATH = CURRENT_DIR + "/Output/";
	public static final String WIN_PATH = CURRENT_DIR + "\\Output\\";
	
	// Output dir for writing files.
	public static final String OUTPUT_PATH = "";
	
	public static final String SPLIT_CHAR = "";
	
	
}


