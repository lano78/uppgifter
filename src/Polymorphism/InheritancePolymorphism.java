package Polymorphism;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InheritancePolymorphism {

	public InheritancePolymorphism() {
		
		Store store = new Store("Motorvaruhuset", "Storgatan 10", "0701231232", "http://www.motorvaruhuset.se");
		Order order = null;
		Customer customer = null;
		
		
		order = new Order();
		order.setOrderNumber(1234);
		order.setOrderDate(new Date());
		order.setCustomer(new Customer("Lars", "Nordström", "0701234567", "lars@mail.com"));
		order.setVehicle(new SportsCar(4, "Black", "Batmobile", 870));
		store.sellVehicle(order);
		
//		store.saveInvoice(new Invoice(1000000.50,order, store));
//		order = null;
//		//System.out.println(store.getInvoice(1).toString());
//		
//		// Sell a new Vehicle
//		store.sellVehicle(
//				new Order(
//					3412, 
//					new Date(),
//					new Customer("Morgan", "Gideryd", "0701232233", "i_am_a_tank@mail.com"), 
//					new Truck(12, 50000)
//				)
//		);
//		
//		// Save the Invoice
//		store.saveInvoice(new Invoice(589000 ,store.getOrder(store.getOrders().size() - 1 ), store));
//		//System.out.println(store.getInvoice(2).toString());
//		
//		// Create a customer, order, invoice and sell a Vehicle.
//		customer = new Customer("Viktor", "Lindh", "0701112233", "viktor@mail.com");
//		
//		Employee employee = new Employee(store, customer);
//		//System.out.println(employee.toString());
//		
//		Bicycle bicycle = new Bicycle(1, "Rosa", false, 2);
//		
//		order = new Order();
//		order.setOrderNumber(2341);
//		order.setOrderDate(new Date());
//		order.setCustomer(customer);
//		
//		order.setVehicle(bicycle);
		
//		store.sellVehicle(order);
		Invoice invoice = new Invoice(799.90, order, store);
//		System.out.println(invoice);
		
		
		store.saveInvoice(invoice);
//		System.out.println(store.getInvoice(1));
		
		order = null;
//		store.deSerializeInvoice();
		//invoice = store.deSerializeInvoice();
		//System.out.println(invoice);
		
//		invoice = store.getInvoice(3);
//		System.out.println(invoice);
		
		// Print all orders made.
		for(Order o : store.getOrders()) {
			//System.out.println(o.toString() + "\r");
		}
		
		// Print all Invoices saved
		for (Map.Entry<Integer, Invoice> item : store.getAllInvoices().entrySet() ) {
//			System.out.println(item.getKey() + ". " + item.getValue().toString() + "\n");
			System.out.println();
		}
		
		// Deliver a Vehicle from an Invoice by getting the Vehicle in the order of the Invoice.
//		Vehicle deliver = store.deliverVehicle(store.getInvoice(1));
//		System.out.println(deliver);
		


	}
	


	public static void main( String[] args ) {
		try {
			System.out.println(System.getProperty("user.dir"));
			new InheritancePolymorphism();
			
		}catch(Exception ex) {
			
			System.out.println(ex.getMessage());
		}
	}
	
	//-------------------------------------------------------------------------------------------
	// 
	//-------------------------------------------------------------------------------------------
	
	
	//-------------------------------------------------------------------------------------------
	// RetailStoreContract interface.
	//-------------------------------------------------------------------------------------------
	public interface RetailStoreContract{
		public void sellVehicle(Order order);
		public void saveInvoice(Invoice invoice);
		public Vehicle deliverVehicle(Invoice invoice);
		public List<Order> getOrders();
//		public void serializeInvoice(Invoice invoice);
		public void serializeInvoice(Object invoice);
		public void deSerializeInvoice();
		public void saveSerializedInvoices(Invoice invoice);
	}
	//-------------------------------------------------------------------------------------------
	// InvoiceDesignContract
	//-------------------------------------------------------------------------------------------
	public interface InvoiceDesignContract {
		
	}
	//-------------------------------------------------------------------------------------------
	//	Store Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Store implements RetailStoreContract, Serializable {
	
		

		private String name, address, phone, website;
		
		private ArrayList<Order> orders = new ArrayList<Order>();
		private Map<Integer, Invoice> invoices = new HashMap<Integer, Invoice>();
		private Map<Integer, Employee> employees = new HashMap<Integer, Employee>();
		
		/**
		 * Default Constructor<br>
		 * Sets all class variables to null
		 */
		public Store() {
			this.setStoreName(null);
			this.setAddress(null);
			this.setPhone(null);
			this.setWebsite(null);
		}
		
		/**
		 * Parameterized Constructor.
		 * @param storeName
		 * @param address
		 * @param phone
		 * @param website
		 */
		public Store(String storeName, String address, String phone, String website) {
			this.setStoreName(storeName);
			this.setAddress(address);
			this.setPhone(phone);
			this.setWebsite(website);
		}
		
		public String getStoreName() {
			return name;
		}
		
		public void setStoreName(String theStoreName) {
			try {
				if(theStoreName.length() != 0) {
					this.name = theStoreName;
				}else {
					throw new IllegalArgumentException("StoreName can not be empty!");
				}
			}catch(Throwable ex) {
				System.out.println(ex.getMessage());
			}
			
		}
		
		/**
		 * @return the address
		 */
		public String getAddress() {
			return address;
		}

		/**
		 * @param theAddress the theAddress to set
		 */
		public void setAddress(String theAddress) {
			
			try {
				if(theAddress.length() != 0) {
					
					this.address = theAddress;
					
				}else {
					
					throw new IllegalArgumentException("Address can not be empty!");
					
				}
				
			}catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
				
			}
		}

		/**
		 * @return the phone
		 */
		public String getPhone() {
			return phone;
		}

		/**
		 * @param thePhone the thePhone to set
		 */
		public void setPhone(String thePhone) {
			
			try {
				if(thePhone.length() != 0) {
					
					this.phone = thePhone;
					
				}else {
					
					throw new IllegalArgumentException("Phone can not be empty!");
					
				}
				
			}catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
				
			}
		}

		/**
		 * @return the website
		 */
		public String getWebsite() {
			return website;
		}

		/**
		 * @param theWebsite the theWebsite to set
		 */
		public void setWebsite(String theWebsite) {
			
			try {
				if(theWebsite.length() != 0) {
					
					this.website = theWebsite;
					
				}else {
					
					throw new IllegalArgumentException("Website can not be empty!");
					
				}
				
			}catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
				
			}
		}

		/**
		 * @return the employees
		 */
		public Map<Integer, Employee> getEmployees() {
			return employees;
		}

		/**
		 * Hire a new employee
		 * @param employee
		 */
		public void hireNewEmployee(Employee employee) {
			if( this.employees.isEmpty() ){
				this.employees.put(1, employee);
			}else {
				this.employees.put(this.employees.size() + 1, employee);
			}
			
		}
		
		/**
		 * Fire an employee
		 * @param key
		 */
		public void fireEmployee(int key) {
			this.employees.remove(key);
		}
		
		/**
		 * Mandatory method from RetailStoreContract.
		 * @param vehicle
		 */
		public void sellVehicle(Order order) {
			 orders.add(order);
		}
		
		public Order getOrder(int index){
			return orders.get(index);
		}
		
		/**
		 * Save an Invoice to the records.
		 * @param invoice
		 */
		public void saveInvoice(Invoice invoice) {
			try {
				if(invoices.isEmpty()) {
					invoices.put(1, invoice);
				}else {
					invoices.put(invoices.size() + 1, invoice);
				}
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
			}	
		}

		public ArrayList<Order> getOrders() {
			return orders;
		}
		
		public Invoice getInvoice(int index) {
			return invoices.get(index);
		}
		
		public Map<Integer, Invoice> getAllInvoices(){
			return invoices;
		}
		
		/**
		 * Get a vehicle from an order by provide the invoice.
		 * @param invoice
		 * @return
		 */
		public Vehicle deliverVehicle(Invoice invoice) {
			return invoice.getOrder().getVehicle();
		}
		
		/**
		 * Serialize an Invoice.
		 * @param invoice
		 */
		public void serializeInvoice(Object invoice) {
			try {
				FileOutputStream file = new FileOutputStream(System.getProperty("user.dir") +"src/Polymorphism/" + invoice.getClass().getSimpleName() +".ser");
				ObjectOutputStream output = new ObjectOutputStream(file);
				output.writeObject(invoice);
				output.close();
				
			}catch(FileNotFoundException ex) {
				ex.printStackTrace();
				
			}catch(IOException ex) {
				ex.printStackTrace();
			}
			
		}
		
		/**
		 * Deserialize an invoice
		 */
		public void deSerializeInvoice() {
			Invoice invoice = null;
			try(FileInputStream file = new FileInputStream("src/Polymorphism/Invoice.ser")) {
				
				ObjectInputStream input = new ObjectInputStream(file);
				
				invoice = (Invoice) input.readObject();
				input.close();
//				System.out.println( (Invoice) input.readObject() );
			}catch(Throwable ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
			System.out.println(invoice);
//			return invoice;
		}
		
		public void saveSerializedInvoices(Invoice invoice) {
			
			
		}
		
		@Override
		public String toString(){
			return  " StoreName : " + this.getStoreName() + ",\r\tAddress : " + this.getAddress() + ",\n\tPhone : " + this.getPhone() + ",\n\tWebsite : " + this.getWebsite();
		}




	}
	
	//-------------------------------------------------------------------------------------------
	// Order Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Order implements Serializable {
				
		private int orderNumber;
		private Date orderDate;
		private Customer customer;
		private Vehicle vehicle;
		
		
		public Order() {
			this.orderNumber = 0;
			this.orderDate = null;
			this.customer = null;
			this.vehicle = null;
		}
		
		public Order(int orderNumber, Date orderDate, Customer customer, Vehicle vehicle) {
			this.setOrderNumber(orderNumber);
			this.setOrderDate(orderDate);
			this.setCustomer(customer);
			this.setVehicle(vehicle);
			
		}
		
		public void setOrderNumber(int number) {
			try {
				
				if( (String.valueOf(number).length() == 4) ) {
				
					this.orderNumber = number;
				
				}else{
				
					throw new IllegalArgumentException("Invalid orderNumber, can only be 4 characters and.");
				}
				
			}catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
			
			}
			
		}
		
		public int getOrderNumber() {
			return orderNumber;
		}
		
		/**
		 * @return the orderDate
		 */
		public Date getOrderDate() {
			return orderDate;
		}

		/**
		 * @param orderDate the orderDate to set
		 */
		public void setOrderDate(Date orderDate) {
			this.orderDate = orderDate;
		}

		/**
		 * @return the customer
		 */
		public Customer getCustomer() {
			return customer;
		}

		/**
		 * @param customer the customer to set
		 */
		public void setCustomer(Customer customer) {
			this.customer = customer;
		}

		public void setVehicle(Vehicle vehicle) {
			this.vehicle = vehicle;
		}
		
		public Vehicle getVehicle() {
			return vehicle;
		}
		
		@Override
		public String toString() {
			return "\t" + this.getClass().getSimpleName() + " : " + this.orderNumber + ",\r\tOrder Date : " + this.getOrderDate() + ",\r" + this.customer.toString() +"\r" + this.vehicle.toString();
		}
	}
	//-------------------------------------------------------------------------------------------
	//	Invoice Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Invoice implements Serializable {
		
		private Order order;
		private Store store;
		private double price;
		
		public Invoice() {
			this.setPrice(0);
			this.setOrder(null);
			this.setStore(null);
		}
		
		public Invoice(double cost, Order order, Store store) {
			this.setPrice(cost);
			this.setOrder(order);
			this.setStore(store);
		}
		
		/**
		 * @return the cost
		 */
		public double getPrice() {
			return price;
		}

		/**
		 * @param cost the cost to set
		 */
		public void setPrice(double cost) {
			this.price = cost;
		}

		/** 
		 * @return the order
		 */
		public Order getOrder() {
			return order;
		}
		
		/**
		 * @param order the order to set
		 */
		public void setOrder(Order order) {
			this.order = order;
		}
		
		/**
		 * @return the customer
		 */
		public Store getStore() {
			return store;
		}
		
		/**
		 * @param customer the customer to set
		 */
		public void setStore(Store store) {
			this.store = store;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return this.getClass().getSimpleName() + " = [\r\tPrice : " + this.getPrice() + ",\n" + this.order.toString() + "\r" + this.store.toString() + "\r]";
		}
			
	}
	//-------------------------------------------------------------------------------------------
	//	Person Class
	//-------------------------------------------------------------------------------------------
	public abstract class Person {
				
		public Person() {
			
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "";
		}
	}
	//-------------------------------------------------------------------------------------------
	//	Customer Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Customer extends Person implements Serializable  {
		

		
		private String fname, lname, phone, email;
		
		public Customer() {
			super();
			this.setFname(null);
			this.setLname(null);
			this.setPhone(null);
			this.setEmail(null);
		}
		
		public Customer(String fname, String lname, String phone, String email) {
			super();
			this.setFname(fname);
			this.setLname(lname);
			this.setPhone(phone);
			this.setEmail(email);
		}

		/**
		 * @return the fname
		 */
		public String getFname() {
			return fname;
		}

		/**
		 * @param fname the fname to set
		 */
		public void setFname(String fname) {
			this.fname = fname;
		}

		/**
		 * @return the lname
		 */
		public String getLname() {
			return lname;
		}

		/**
		 * @param lname the lname to set
		 */
		public void setLname(String lname) {
			this.lname = lname;
		}

		/**
		 * @return the phone
		 */
		public String getPhone() {
			return phone;
		}

		/**
		 * @param phone the phone to set
		 */
		public void setPhone(String phone) {
			this.phone = phone;
		}

		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @param email the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return this.getClass().getSimpleName() + " = [\r\tfname = " + this.getFname() + ",\r\tlname = " + this.getLname() + ",\r\tphone = "
					+ this.getPhone() + ",\r\temail = " + this.getEmail() + "\r]";
		}
		
	}
	//-------------------------------------------------------------------------------------------
	//	Employee Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Employee extends Person implements Serializable {
				
		private Store store;
		private Person person;
		
		public Employee() {
			super();
			this.setStore(null);
			this.setPerson(null);
		}
		
		public Employee(Store theStore, Person thePerson) {
			super();
			this.setStore(theStore);
			this.setPerson(thePerson);
		}
		
		/**
		 * @return the store
		 */
		public Store getStore() {
			return store;
		}

		/**
		 * @param store the store to set
		 */
		public void setStore(Store store) {
			this.store = store;
		}

		/**
		 * @return the person
		 */
		public Person getPerson() {
			return person;
		}

		/**
		 * @param person the person to set
		 */
		public void setPerson(Person person) {
			this.person = person;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return this.getClass().getSimpleName() + " = [\r" + this.getPerson().toString() + "\r\tWorks at : " + this.getStore().toString()  +"\r" + super.toString() + "\n]";
		}
	}
	//-------------------------------------------------------------------------------------------
	//	Product Class
	//-------------------------------------------------------------------------------------------
	public abstract class Base {
		
		public Base() {
			
		}
		
		@Override
		public String toString(){
			return "";
		}
	}
	//-------------------------------------------------------------------------------------------
	//	Product Class
	//-------------------------------------------------------------------------------------------
	public abstract class Product extends Base {
		
		public Product() {
			
		}
		
		@Override
		public String toString(){
			return "";
		}
	}
	
	//-------------------------------------------------------------------------------------------
	//	Vehicle Class
	//-------------------------------------------------------------------------------------------

	public abstract class Vehicle extends Product {
		
		private int wheels;
		private String color;
		
		public Vehicle(){
			this.wheels = 0;
			this.setColor(null);
		}
		

		public Vehicle(int numberOfWheels){
			this.setWheels(numberOfWheels);
		}
		
		public Vehicle(int numberOfWheels, String color){
			this.setWheels(numberOfWheels);
			this.setColor(color);
		}
		
		public void setWheels(int numberOfWheels) {
			
			try{
				
				if(numberOfWheels != 0){
					
					this.wheels = numberOfWheels;
					
				}else{
					
					throw new IllegalArgumentException("A Car without wheels is not Car.");
				}
				
			}catch(Throwable ex){
				
				System.out.println(ex.getMessage());
				
			}
		}

		public int getWheels(){
			return wheels;
		}

		/**
		 * @return the color
		 */
		public String getColor() {
			return color;
		}

		/**
		 * @param color the color to set
		 */
		public void setColor(String color) {
			this.color = color;
		}

		@Override
		public String toString(){
			return " \tWheels : " + this.getWheels() + ",\n\tColor : " + this.getColor() + ",";
		}
	}

	//-------------------------------------------------------------------------------------------
	//	SportsCar Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class SportsCar extends Vehicle implements Serializable {
		
		private int hp;
		private String model;
		
		public SportsCar() {
			super(4);
			this.setHp(500);
			this.setModel(null);
		}

		public SportsCar(int numberOfWheels, String model, int numberOfHp) {
			super(numberOfWheels);
			this.setModel(model);
			this.setHp(numberOfHp);
			
		}
		
		public SportsCar(int numberOfWheels, String color, String model, int numberOfHp) {
			super(numberOfWheels, color);
			this.setModel(model);
			this.setHp(numberOfHp);
		}

		public void setHp(int numberOfHp) {
			
			try{
				
				if(numberOfHp != 0){
					
					this.hp = numberOfHp;
					
				}else{
					
					throw new IllegalArgumentException("A SportsCar without Hp is not SportsCar.");
					
				}
				
			} catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
			}
			
		}

		public int getHp(){
			return hp;
		}

		/**
		 * @return the model
		 */
		public String getModel() {
			return model;
		}

		/**
		 * @param model the model to set
		 */
		public void setModel(String model) {
			this.model = model;
		}

		@Override
		public String toString(){
			return this.getClass().getSimpleName() + " = [\r" + super.toString() + "\n\tModel : " + this.getModel() + ",\r\tHp : " + this.getHp() + "\r]" ;
		}
	}

	//-------------------------------------------------------------------------------------------
	//	Truck Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Truck extends Vehicle implements Serializable {
		
		private int capacity;

		public Truck(){
			super(6);
			this.setCapacity(30000);
		}

		public Truck(int numberOfWheels, int theCapacity){
			super();
			super.setWheels(numberOfWheels);
			this.setCapacity(theCapacity);
		}

		public int getCapacity(){
			return capacity;
		}
		
		public void setCapacity(int maxCapacity){
			
			try{
				
				if(maxCapacity != 0){
					
					this.capacity = maxCapacity;
					
				}else{
					
					throw new IllegalArgumentException("Truck must have a capacity or it can't possibly be a Truck.");
					
				}
			}catch(Throwable ex){
				
				System.out.println(ex.getMessage());
			}
			
		}

		@Override
		public String toString(){
			return this.getClass().getSimpleName() + " = [\r" + super.toString() + "\r\tCapacity : " + this.getCapacity() + "\r]" ;
		}

	}


	//-------------------------------------------------------------------------------------------
	// Bicycle Class
	//-------------------------------------------------------------------------------------------
	@SuppressWarnings("serial")
	public class Bicycle extends Vehicle  implements Serializable {
		
		private boolean hasEngine;
		private int numPedals;
		private String typeOfBicycle;
		
		public Bicycle() {
			super(2);
			this.hasEngine = false;
			this.numPedals = 2;
			this.typeOfBicycle = "Normal bicycle";
		}
		
		public Bicycle(int numberOfWheels, String color, boolean hasAnEngine, int numberOfPedals) {
			super(numberOfWheels, color);
			this.setHasEngine(hasAnEngine);
			this.setNumPedals(numberOfPedals);
		}
		
		public void setNumPedals(int numberOfPedals) {
			
			try {
				
				if(super.getWheels() == 1) {
					
					this.numPedals = numberOfPedals;
					this.setTypeOfBicycle("Unicycle");
					
				}else if(numberOfPedals == 2) {
					
					this.numPedals = numberOfPedals;
					this.setTypeOfBicycle("Normal bicycle");
					
				}else if(numberOfPedals == 4) {
					
					this.numPedals = numberOfPedals;
					this.setTypeOfBicycle("Tandem bicycle");
					
				}else {
					
					throw new IllegalArgumentException("A bicycle must have pedals!");
					
				}
			}catch(Throwable ex) {
				
				System.out.println(ex.getMessage());
			}
		}
		
		public int getNumPedals() {
			return numPedals;
		}
		
		public void setTypeOfBicycle(String typeOfBike) {
			this.typeOfBicycle = typeOfBike;
		}
		
		public String getTypeOfBicycle() {
			return typeOfBicycle;
		}
		
		public void setHasEngine(boolean hasAnEngine) {
			
			try {
				
				if(hasAnEngine && ( super.getWheels() > 2 ) ) {
					
					throw new IllegalArgumentException("A bicycle can't have an engine, it's a motorcycle!");
					
				}else {
					
					this.hasEngine = hasAnEngine;
					
				}
			}catch(Throwable ex){
				
				System.out.println(ex.getMessage());
				
			}
			this.hasEngine = hasAnEngine;
		}
		
		public boolean getHasEngine() {
			return hasEngine;
		}
		
		@Override
		public String toString(){
			return this.getClass().getSimpleName() + " = [\r" + super.toString() + "\r\tPedals : " + this.getNumPedals() + "\r\tModel : " + this.getTypeOfBicycle() + "\r\tHasAnEngine : " + this.getHasEngine() + "\r]" ;
		}
	}



}


