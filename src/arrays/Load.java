package arrays;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Load {

	public Load() {
		
	}
	/*
	 * This method reads a file from a specified path and return the contents as a string.
	 */
	public String readFileToString(String fileName) {
		
		StringBuilder sb = new StringBuilder();
		
		try (FileReader fileReader = new FileReader(new File(fileName))) {
			
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while (bufferedReader.ready()) {

				String line = bufferedReader.readLine();
				sb.append(line);

			}
			
			bufferedReader.close();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return sb.toString();
	}

}
