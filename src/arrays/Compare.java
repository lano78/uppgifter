package arrays;

public class Compare {

	public Compare() {
		
	}
	
	public boolean compare(String a, String b) {
		return a.length() > b.length();
	}
	
	// For using Comparator
//	public int compare(String a, String b) {
//		if(a.length() > b.length()) {
//			return 1;
//		}else if(a.length() < b.length()){
//			return -1;
//		}
//		return 0;
//	}

}
