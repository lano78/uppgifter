package arrays;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.SortedMap;
import java.util.Map.Entry;

/**
 * Helper class for write data to files
 * @author lano78
 * @version 1.0
 */
public class Save {

	public Save() {
		
	}
	
	/**
	 * Write a string to file as a single line or as string is formatted.
	 * @param fileName as String
	 * @param content as String
	 */
	public void writeToFile(String fileName, String content) {
		
		try (FileOutputStream out = new FileOutputStream(new File(fileName) ) ) {
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			
			bw.write(content);
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes the contets of an String[] on separate lines to a file.
	 * @param fileName as String
	 * @param content as String[] 
	 */
	public void writeToFile(String fileName, String [] content) {
		
		try (FileOutputStream out = new FileOutputStream(new File(fileName) ) ) {
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			for(String s : content) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes the contents of an ArrayList<String> line by line to a file.
	 * @param fileName as String.
	 * @param content.
	 */
	public void writeToFile(String fileName, List<String> content) {
		
		try (FileOutputStream out = new FileOutputStream(new File(fileName) ) ) {
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			for(String s : content) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes the contents of a SortedMap<Integer, String> value line by line to file.
	 * @param fileName as String
	 * @param content the value as Map.
	 * @param includeKey boolean, if the key should be written to file or not
	 */
	public void writeToFile(String fileName, SortedMap<Integer, String> content, boolean includeKey) {
		
		try (FileOutputStream out = new FileOutputStream(new File(fileName) ) ) {
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			for(Entry<Integer, String> s : content.entrySet()) {
				if(!includeKey) {
				bw.write(s.getValue());
				bw.newLine();
				}else {
					bw.write(s.getKey() + " " + s.getValue());
					bw.newLine();
				}
			}
			bw.flush();
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


