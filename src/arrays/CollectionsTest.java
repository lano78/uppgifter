package arrays;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class CollectionsTest {
	
	
	char dir;
	List<String> wordList = new ArrayList<String>();
	
	SortedMap<Integer, String> treeMap = new TreeMap<Integer, String>();
	
	StringBuilder builder = new StringBuilder();

	String regex = "";
	Pattern pattern = Pattern.compile(regex);
	
	String cleanRegex = "[\\s\\,.!]";
	Pattern cp = Pattern.compile(cleanRegex);
	
	// Constructor
	public CollectionsTest() {

		this.load("D:\\Dropbox\\Developer\\Java\\Uppgifter\\src\\arrays\\input.txt");
//		this.sortedWordlist(wordList);
		this.sortedTreeMap(treeMap);
//		printWordlist();
//		System.out.println(treeMap.get(13));
		
		
	}

	public static void main(String[] args) {
		new CollectionsTest();

	}

	public void save() {

	}
	
	public void sort(String order) {
		if(order.equalsIgnoreCase("hi")) {
			dir = '<';
		}else if(order.equalsIgnoreCase("low")) {
			dir = '>';
		}
	}
	public boolean compare(String a, String b) {
		return a.length() > b.length();
	}

	public void load(String fileName) {
		try (FileReader fileReader = new FileReader(new File(formatPathToFile(fileName)))) {
			
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while (bufferedReader.ready()) {
				String line = this.cleanString(bufferedReader.readLine() );
				String[] words = line.split("\\s+");
				for (String s : words) {
					if(!s.equalsIgnoreCase("")) {
						
						// Populate the TreeMap
						this.populateTreeMap(s);
						
						// Populate the ArrayList with words
						this.populateArrayList(s);
					}

				}

			}
			
			bufferedReader.close();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	// Method for adding words to the TreeMap.
	public void populateTreeMap(String s) {
		if (treeMap.isEmpty()) {
			treeMap.put(1, s);
		} else {
			treeMap.put(treeMap.lastKey() + 1, s);
		}
		
	}
	// Method for adding words to ArrayList.
	public void populateArrayList(String s) {
		wordList.add(s);
	}
	
	public void printWordlist() {
		System.out.println("\rSize\tWord");
		System.out.println("------------------------");
		for(String s : this.wordList) {
			System.out.println(" " + s.length() + "\t " + s );
		}
		
	}
	//
	public void swap(int a, int b) {
		String ta = wordList.get(a);
		String tb = wordList.get(b);
		wordList.set(a, tb);
		wordList.set(b, ta);
		System.out.println(" Swap : " + a + ", " + ta + " -> " + b +", " + tb );

	}
	
	// memory heap....
	public void sortedWordlist(List<String> wl) {
		
		int swapsMade = 0;

		System.out.println("\r========================================");
		System.out.println("\tSorting Started");
		System.out.println("========================================\r");
		
		for(int i = wl.size() - 1 ; i > 1; i--) {
			
			for(int j = 0; j < i; j++) {	
				System.out.println( i + ", " + j );
				if(wl.get(j).length() < wl.get(j+1).length()) {
					swapsMade++;
//					System.out.println( j + ", " + (j+1) );
//					this.swap(j, j+1);
					String ta = wordList.get(j);
					String tb = wordList.get(j+1);
					wordList.set(j, tb);
					wordList.set(j+1, ta);
//					System.out.println(" Swap : " + j + ", " + ta + " -> " + (j+1) +", " + tb );
				}
			}
			
		}
		System.out.println("\r========================================");
		System.out.println(" Sorting Completed\r Word Count : " + wl.size() + "\r Swap Count : " + swapsMade);
		System.out.println("========================================");
	}
	
	public void printTreeMap() {
		System.out.println("\rSize\tWord");
		System.out.println("------------------------");
		for(Entry<Integer, String> s : this.treeMap.entrySet()) {
			System.out.println(" " + s.getValue().length() + "\t " + s.getValue() );
		}
	}
	// Sorting the treeMap based on value length.
	public void sortedTreeMap(SortedMap<Integer, String> tree) {
		int swapsMade = 0;

		System.out.println("\r========================================");
		System.out.println("\tSorting Started");
		System.out.println("========================================\r");
		for(int i = tree.size(); i > 1; i--) {
			
			for(int j = 1; j < i; j++) {
				
				if( tree.get(j).length() < tree.get(j+1).length() ) {
					swapsMade++;
//					System.out.println( j + ", " + (j+1) );
					String oldValue = tree.get(j);
					String newValue = tree.get(j+1);
					
					tree.replace(j, oldValue, newValue);
					tree.replace(j+1, newValue, oldValue);
					
					System.out.println(" Swap : " + j + ", " + oldValue + " -> " + (j+1) +", " + newValue );
				}
			}
		}
		System.out.println("\r========================================");
		System.out.println(" Sorting Completed\r Word Count : " + tree.size() + "\r Swap Count : " + swapsMade);
		System.out.println("========================================");
		
		this.printTreeMap();
	}
	
	//
	public int getCharCount(String s, char find) {
		int cnt = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != find)
				continue;
			cnt++;
		}
		return cnt;
	}

	//
	public int getCharCount(String s) {
		return s.length();
	}

	//
	public String formatPathToFile(String path) {
		return path.replaceAll("/", "\\\\");
	}

	public String cleanString(String s) {
		return s.replaceAll(cleanRegex, " ");
	}

//	@Override
//	public int compare(String o1, String o2) {
//		
//		return 0;
//	}

}
