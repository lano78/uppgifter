package arrays;

/**
 * 
 * @author lano78
 * @version 1.0
 */
public class Time {

	public Time() {
		
	}
	/**
	 * This method calculates the elapsed time between now and then.
	 * @param now current time in miliseconds.
	 * @return diff (then - now)
	 */
	public static long getElapsedTime(long now) {
		return (System.currentTimeMillis() - now);
	}

}
