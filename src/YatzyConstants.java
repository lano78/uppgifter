
public interface YatzyConstants extends Device{
	
	// Variables
	public static final int NUM_DICE 	= 5;
	public static final int NUM_TURNS 	= 3;
	public static final int NUM_ROUNDS 	= 15;
	
	// bonus threshold
	public static final int THRESHOLD_BONUS    = 63;
	
	// Score values
	public static final int VAL_ONES 		   = 1;
	public static final int VAL_TWOS 		   = 2;
	public static final int VAL_THREES 		   = 3;
	public static final int VAL_FOURS 		   = 4;
	public static final int VAL_FIVES 		   = 5;
	public static final int VAL_SIXES 		   = 6;
	
	public static final int VAL_BONUS 		   = 50;
	public static final int VAL_YATZY 		   = 50;
	public static final int VAL_SMALL_STRAIGHT = 15;
	public static final int VAL_LARGE_STRAIGHT = 20;

	// Score card categories
	public String [] SCORE_CARD = {"Ettor", "Tvåor", "Treor", "Fyror", "Femmor", "Sexor", "Summa", "Bonus", "1 Par", "2 Par", "Triss", "Fyrtal", "Liten", "Stor", "Chans", "Yatzy", "Totalt"};
	

}
