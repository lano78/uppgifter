//---------------------------------------------
// NamnListan.java
// Matar in namn och nummer tills man stoppar loopen.
//
//---------------------------------------------
import java.util.*;
import java.io.*;

public class NamnListan implements SecretSauce{

	public static void main(String[] args) {
		
		// Init the scanner so we can listen for user input. 
		Scanner input = new Scanner(System.in);
		
		// Path to where we want to write our file.
		String filePath = CURRENT_DIR + MAC_PATH; // local url from SecretSauce...
		String fileName = "namnlistan.txt";
		
		
		// count for keeping track how many entries we have in our arrays.
		int currentCnt = 0;

		// How many entries in our arrays we want, set this because of memory restrictions.
		int entries = 3;

		// our arrays.
		String [] f_Namn = new String[entries];
		String [] e_Namn = new String[entries];
		String [] m_Nummer = new String[entries];

		// Our input strings (förnamn, efternamn, mobilnr) 
		String f,e,m;

		// input variable for saving the list later.
		String s;
		
		// state of our program. true = looping, false = paused until we have typed our entries.
		boolean isRunning = true;

		// the loop
		do{
			// set isRunning to false, (pause the loop until we have typed all entries)
			isRunning = false;

			// our userInput for what to do.
			System.out.println("Vad heter du i förnamn?");
			f = input.next();

			// carry on if we didn't type stop.
			if (  !f.equals("stop") ) {
				System.out.println("Vad heter du i efternamn?");
				e = input.next();

				System.out.println("Vad har du för mobilnr?");
				m = input.next();


				// insert the inputs in the arrays
				f_Namn[currentCnt] = f;
				e_Namn[currentCnt] = e;
				m_Nummer[currentCnt] = m;
			}


			// un-pause the loop so we can do it again.
			isRunning = true;

			// check if we have all entries	(our arrays are full) or we told the loop to stop.
			if ( ( currentCnt <= entries ) || (  f.equals("stop") ) ){

				// pause the loop.
				isRunning = false;

				// pretty header for our print records.
				System.out.println("-----------------------------");
				System.out.println("\tListan");
				System.out.println("-----------------------------");

				// variable for printing the records entered, -1 because we don't want to print the "stop" or print null records.
				int numRecords = (currentCnt - 1);

				// loop through the arrays
				for ( int i = 0; i <= numRecords; i++ ){

					// print the array values.
					System.out.println( " " + (i + 1) + ". " + f_Namn[i] + "\t" + e_Namn[i] + "\t" + m_Nummer[i]  );

					// if we have looped through all entries.
					if (i == numRecords){
						// End of our record.
						System.out.println("-----------------------------");

						// exit the program when done
						//System.exit(0);
					}
				}
				// Ask if we want to save the list to a file
				System.out.println("Spara listan till ett dokument? y/n");
				// wait for input (y/n)
				s = input.next();
				if( s.equals("y") ){
					
					// false = write a new list, true = add more names.
					boolean addToFile = false;
					
					File fPath = new File(filePath+fileName);
					// if there's a file written, ask if we want to overwrite or add more names to the file.
					if ( fPath.isFile() && fPath.canRead() ){
						System.out.println("Filen " + fileName + " finns redan, vill du fylla på med fler namn? y/n");
						String answer = input.next();

	
						// We want to add more names to the list
						if( answer.equals("y") ){
							System.out.println("Lägger till namnen....");
							addToFile = true;
						}else{
							System.out.println("Skriver en ny lista....");
							addToFile = false;
						}
					}
					
					try{
						
						FileWriter fWriter = new FileWriter(fPath, addToFile);
						PrintWriter pWriter = new PrintWriter(fWriter);

						for ( int i = 0; i <= numRecords; i++ ){
							pWriter.println( (i + 1) + ". " + f_Namn[i] + "\t" + e_Namn[i] + "\t" + m_Nummer[i] );
						}
						
						// Close PrintWriter to avoid memory leaks.
						pWriter.close();
						
						// Tell the us we are done.
						System.out.println("Listan färdig.");
						
					}catch(IOException ex){
 
					}

				} else{
					// exit the program when done
					System.out.println("Avslutar programmet...");
					System.exit(0);
				}
			}

			// increment the count so we insert next round in the next empty memory slot.
			currentCnt++;

			// loop until program state changes or we tell it to stop.	
		}while(isRunning || f.equals("Stop") );

		// close the scanner to avoid memory leaks.
		input.close();
	}

}






