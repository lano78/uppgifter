package LottoUppgiften;

import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;


public class Class_Ticket implements Constants {

	// Scanner
	Scanner read;

	// Array for our lotto numbers (7+4).
	int [] lottoArray = new int[NUM_COUNT + NUM_EXTRAS];

	// Array for the my numbers
	int [] myNum = new int[NUM_COUNT];

	// position count for the array
	int count = 0;

	// init the number class
	Class_Number number = new Class_Number();

	// index
	final int IDX = (lottoArray.length - 1);


	// Get a specific number
	public int getNumber(int pos){
		return this.lottoArray[pos];
	}

	// insert to array
	public  void insertToArray(int pos, int num){
		this.lottoArray[pos] = num;

	}

	// create the winning numbers.
	public void createLottoTicket(){
		int temp = 0;

		// loop for generating the lotto numbers,.
		do{

			// temp number for our random, overwritten each time loop runs. 
			
			for(int i = 0; i <= IDX; i++){
				
				temp = number.generateNumber();
				
				if (!(lottoArray[i] == temp) && (temp < 36) ){
				//System.out.println("Unikt nummer");
				
				// its unique so we insert it to the array.
				lottoArray[count] = temp;
				
				// increment index (if we have a match)
				count++;
				
				// get out...
				//break;
				}
				
			}
			/*
			if (lottoArray[count] == temp ){

				System.out.println("Numret finns redan i arrayen...");

			}else{

				// insert our unique in the array
				lottoArray[count] = temp;

				// increment the count each time we have a unique number
				count++;
			}
			*/
			// loop until we have 11 unique numbers.
		}while(count <= IDX);

	}

	public void showResult(){
		int num = 0;
		int bonus = 0;
		int count = 0;
		
		// compare the first 7 numbers
			for(int i = 0; i<= 6; i++){
				for(int j = 0; j<= 6; j++){
					if (lottoArray[i] == myNum[j]){
						num++;
					}
					
				}
				
			}
			// Check the last 4
			for(int i = 7; i <= 10; i++){
				for(int j = 0; j <= 6; j++){
					if (lottoArray[i] == myNum[j]){
						bonus++;
					}
				}
			}
		System.out.println(); 
		System.out.println("Du hade " + num + " + " + bonus + " rätt");
	}


	// Method for reading the contents of the file
	public void readMyNumbers(){
		// path to our saved file.
		String path = "";
		
		if (IS_WIN){
			path = WIN_PATH; 
		} 
		if (IS_MAC){
			path = MAC_PATH; 
		} 
		if (IS_WIN){
			path = UNIX_PATH; 
		}
		// load the file
		
		File file = new File(path + FILENAME);
		
		try {
			read = new Scanner(file);
			System.out.println("Dina lottonummer:  ");
			// loop through the contents of the file
			for(int i = 0; i <= ( myNum.length - 1); i++){
				// insert the contents from the file into an array.
				myNum[i] = read.nextInt();
				// print the numbers in one line
				System.out.print(myNum[i] + " ");
			}
			// Just for a new line in the cmd.
			System.out.println("");
			// catch any errors, file not found etc
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}
	// Show the lotto numbers
	public void showLottoNumbers(){
		System.out.println("");
		System.out.println("Rätt Lotto nummer + tilläggsnummer:  ");
		for (int i = 0; i <= 6; i++){
			System.out.print( lottoArray[i] + " " );
		}
		System.out.print(" +  ");
		for (int i = 7; i <= 10; i++){
			System.out.print( lottoArray[i] + " " );
		}
	}

	// main method to start the drawing.
	public void lottoTicket(){
		readMyNumbers();
		createLottoTicket();
		showLottoNumbers();
		showResult();
		
		System.out.println();
		System.out.println("Avslutar programmet...");
		System.exit(0);
	}


}






