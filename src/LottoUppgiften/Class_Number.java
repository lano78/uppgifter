
package LottoUppgiften;

import java.util.Random;

public class Class_Number implements Constants {
	
	// Better random
	long SEED = System.currentTimeMillis();
	Random mRand = new Random(SEED);
	
	// declare the random number
	private int VALUE = 0;
	
	// Generate a random number.
	public int generateNumber(){
		this.VALUE = (mRand.nextInt(MAX_NUM) + 1);
		return VALUE;
	}
	
	// reset
	public void resetNumber(){
		this.VALUE = 0;
		
	}
	
	
	
	
}






