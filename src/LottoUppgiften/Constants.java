package LottoUppgiften;

public interface Constants {
	// Check
	String OS = System.getProperty("os.name").toLowerCase();
	// Check what platform
	boolean IS_WIN = (OS.indexOf("win") >= 0);
	boolean IS_MAC = (OS.indexOf("mac") >= 0);
	boolean IS_UNIX = (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	// Global Constants
	String CURRENT_DIR = System.getProperty("user.dir");
	
	public static final String FILENAME = "Lotto.txt";
	public static final String PATH_FOR_FILE = "C:\\Users\\Elev\\Dropbox\\Developer\\Java\\Uppgifter\\Output\\";
	public static final String WIN_PATH = "C:\\Users\\Elev\\Dropbox\\Developer\\Java\\Uppgifter\\Output\\";
	public static final String MAC_PATH = "/Users/larsnordstrom/Desktop/Dropbox/Developer/Java/Uppgifter/Output/";
	public static final String UNIX_PATH = "/Users/larsnordstrom/Desktop/Dropbox/Developer/Java/Uppgifter/Output/";
	
	// Output folder name.
	public static final String OUTPUT_FOLDER = "Output";		
			
	// max number count.
	public static final  int MAX_NUM = 35;

	// Kupong Constants	
	public static final int NUM_COUNT = 7;
	public static final int NUM_EXTRAS = 4;
	// 
	
	
	
}
