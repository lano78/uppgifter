package LottoUppgiften;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;


public class UserInput implements Constants{

	// Array for our picked numbers (7)
	int [] numbers = new int[NUM_COUNT];

	// Count for how many numbers we have entered.
	int index = 0;

	// Name of the file to save ( Constants.java )
	String fileName = FILENAME;
	
	// method for writing the numbers to file when we are done.
	private void write(){
		
		// path to our saved file.
		String path = "";
		
		// Check what os we are on and pick correct output path ( see Constants.java)
		if (IS_WIN){
			path = WIN_PATH; 
		} 
		if (IS_MAC){
			path = MAC_PATH; 
		} 
		if (IS_WIN){
			path = UNIX_PATH; 
		}
		// forward declaration
		PrintWriter output;
		// try block for writing file
		try {

			// init the printwriter.
			output = new PrintWriter(path + fileName);

			// loop for itterating through the array
			for( int i = 0; i <= (NUM_COUNT -1); i++){

				// print the numbers in a line so we can see them. 
				System.out.print(numbers[i] + " ");

				// write the numbers to file.
				output.println(numbers[i]);		
			}
			System.out.println();
			System.out.println("Lottoraden sparad som : " +  FILENAME);
			// close the printwriter when we are done
			output.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		// Avslutar programmet

		System.out.println("Avslutar programmet...");
		System.exit(0);
	}
	
	// Method for picking numbers.
	public void pick(){
		// init the scanner so we can catch keyboard input.
		Scanner input = new Scanner(System.in);

		// Pick numbers
		System.out.println("Välj dina 7 olika nummer mellan 1-35.");
		
		// Game loop
		do{
			// Store the users input in a temp int.
			int temp = input.nextInt();
			
			// check if number have already been entered.
			for (int i = 0; i <= 6;){
				
				// do we have the number in the array and is it less than 36?
				if (!(numbers[i] == temp) && (temp < 36) ){
					System.out.println("Unikt nummer");
					
					// its unique so we insert it to the array.
					numbers[index] = temp;
					
					// increment index (if we have a match)
					index++;
					
					// get out...
					break;
				}
				// Exit the loop.
				break;
			}
		
		// Print the index
		System.out.println("Index : " + index);

		// loop until we have 7 numbers
		}while(index < NUM_COUNT);

		// Close scanner to avoid memory leaks. 
		input.close();

		// tell us what numbers have been picked
		System.out.println("Din lottorad är : ");
		
		write();
	}

}
